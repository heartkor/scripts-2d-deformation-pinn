# functions to load results and plot the deformation/active tension/LAT fields

from matplotlib import pyplot as plt
import numpy as np
from matplotlib.ticker import MaxNLocator
import sys
import os
sys.path.append("..")
from PINN.functions.functions_sim_data import load_data

def load_results(t_range,dir,run,star,txy_dir,data_dir):

    """
    load precited and true fields for U, Ta as well as the coordinates of the data points
    if star = True: load X_input and U_input as arrays (array of locations) instead of grid data
    so it can plotted it as scatter data and not pcolormesh

    dir: directory where the results are stored (pred)
    txy_dir: directory where the txy_array file is stored
    data_dir: directory where the true data is stored
    """

    if run!= -1:
        pred_file = dir + 'run_{}/pred.npy'.format(run)
        real_file = txy_dir + 'real.npy'
        X_input_file = dir + 'run_{}/X_input.npy'.format(run)
        U_input_file = dir + 'run_{}/U_input.npy'.format(run)
        txy_array_file = txy_dir + 'txy_array.npy'

    else: # if the correct directory is immeditatly given
        pred_file = dir + 'pred.npy'
        real_file = txy_dir + 'real.npy'
        X_input_file = data_dir + 'X_input.npy'
        U_input_file = data_dir + 'U_input.npy'
        txy_array_file = txy_dir + 'txy_array.npy'

    t_start,t_end = t_range

    pred,real,X_input,U_input,txy_array = np.load(pred_file), np.load(real_file), np.load(X_input_file),np.load(U_input_file), np.load(txy_array_file)
    N_x = int(np.sqrt(len(txy_array)/len(pred[0])))
    txy_grid = txy_array.reshape((len(pred[0]),N_x,N_x,3))

    n_x = int(np.sqrt(len(X_input)/(len(real[0]))))
    if star:
        X_input_grid = X_input.reshape((len(real[0]),-1,3))
        U_input_grid = U_input.reshape((len(real[0]),-1))
        X_input_grid = X_input_grid[t_start:t_end,:,:]
        U_input_grid = U_input_grid[t_start:t_end,:]
    else:
        X_input_grid = X_input.reshape((len(real[0]),n_x,n_x,3))
        U_input_grid = U_input.reshape((len(real[0]),n_x,n_x,2))
        X_input_grid = X_input_grid[t_start:t_end,:,:,:]
        U_input_grid = U_input_grid[t_start:t_end,:,:,:]

    pred = pred[:,t_start:t_end,:,:]
    real = real[:,t_start:t_end,:,:]
    txy_grid = txy_grid[t_start:t_end,:,:,:]

    return [pred,real,X_input_grid,U_input_grid,txy_grid]

def max_abs(d):
        return np.nanmax([abs(np.nanmin(d)),np.nanmax(d)])

def plot_data(pattern,pattern_n,t_range,x_range,t_snaps,dir_data,save,dir_save,save_name,interpol=False):

    """
    plotting timesnaps of the data with the timesteps as columns and the rows are [v,Ta,Ux,Uy]
    """

    u_grid,T_grid,coord_grid,x0s,y0s,txy_grid,t_x_y_array,times,v_grid = load_data(pattern,pattern_n,t_range,x_range,dir_data)

    N = int(np.sqrt(len(T_grid[0])))
    u_grid = u_grid.reshape(len(u_grid),N,N,3)
    T_grid = T_grid.reshape(len(T_grid),N,N)
    v_grid = v_grid.reshape(len(v_grid),N,N)
    x,y = txy_grid[1,0,:,:],txy_grid[2,0,:,:]
    times = txy_grid[0,:,0,0]

    n_t = len(t_snaps)

    relevant_v = v_grid[np.array(t_snaps),:,:]
    relevant_T = T_grid[np.array(t_snaps),:,:]
    relevant_u = u_grid[np.array(t_snaps),:,:,:]
    v_range = [np.nanmin(relevant_v),np.nanmax(relevant_v)]
    T_range = [np.nanmin(relevant_T),np.nanmax(relevant_T)]
    u_mM = np.nanmax(np.abs(np.array([np.nanmin(relevant_u),np.nanmax(relevant_u)])))
    u_range = [-u_mM,u_mM]

    from scipy.interpolate import RegularGridInterpolator
    new_x,new_y = np.linspace(0, 100, 100), np.linspace(0, 100, 100)
    new_xx,new_yy = np.meshgrid(new_x,new_y,indexing='ij')
    
    x, y = np.linspace(0,100,40),np.linspace(0,100,40)
    xx,yy = np.meshgrid(x,y,indexing = 'ij')

    figure_w = n_t*5.5
    figure_l = 20

    fig, axs = plt.subplots(4,n_t, sharex=True, sharey=True)
    fig.set_size_inches(figure_w, figure_l)

    cmaps = ['viridis','magma','BrBG']
    time_font = 30
    time_pad = 15
    cb_font = 30
    cb_pad = 40
    cb_tick_font = 20

    xlabel_font = 25
    x_tick_font = 20

    for col,t in enumerate(t_snaps):

        if interpol:
        
            interp_func = RegularGridInterpolator((x,y), v_grid[t,:,:], method='linear')
            new_v = interp_func((new_xx,new_yy))
            interp_func = RegularGridInterpolator((x,y), T_grid[t,:,:], method='linear')
            new_T = interp_func((new_xx,new_yy))
            interp_func = RegularGridInterpolator((x,y), u_grid[t,:,:,0], method='linear')
            new_ux = interp_func((new_xx,new_yy))
            interp_func = RegularGridInterpolator((x,y), u_grid[t,:,:,1], method='linear')
            new_uy = interp_func((new_xx,new_yy))

            c1 = axs[0][col].pcolormesh(new_xx,new_yy,new_v[:,:],cmap=cmaps[0],vmin=v_range[0],vmax=v_range[1])
            c2 = axs[1][col].pcolormesh(new_xx,new_yy,new_T[:,:],cmap=cmaps[1],vmin=T_range[0],vmax=T_range[1])
            c3 = axs[2][col].pcolormesh(new_xx,new_yy,new_ux,cmap=cmaps[2],vmin=u_range[0],vmax=u_range[1])
            c4 = axs[3][col].pcolormesh(new_xx,new_yy,new_uy,cmap=cmaps[2],vmin=u_range[0],vmax=u_range[1])
            axs[0][col].set_title('t = {}'.format(times[t]),fontsize = time_font,pad = time_pad,fontweight='bold')
        
        else:

            c1 = axs[0][col].pcolormesh(xx,yy,v_grid[t,:,:],cmap=cmaps[0],vmin=v_range[0],vmax=v_range[1])
            c2 = axs[1][col].pcolormesh(xx,yy,T_grid[t,:,:],cmap=cmaps[1],vmin=T_range[0],vmax=T_range[1])
            c3 = axs[2][col].pcolormesh(xx,yy,u_grid[t,:,:,0],cmap=cmaps[2],vmin=u_range[0],vmax=u_range[1])
            c4 = axs[3][col].pcolormesh(xx,yy,u_grid[t,:,:,1],cmap=cmaps[2],vmin=u_range[0],vmax=u_range[1])
            axs[0][col].set_title('t = {}'.format(round(times[t])),fontsize = time_font,pad = time_pad,fontweight='bold')

        for i in range(4):
            axs[i][col].tick_params(axis='both',labelsize=x_tick_font)
        
    axs[0][0].set_yticks([0,50,100])
    axs[0][0].set_xticks([0,50,100])

    fig.tight_layout()
    
    fig.subplots_adjust(right=0.9)
    cs = [c1,c2,c3,c4]
    cblabels = ['v',r'$T_a$',r'$U_x$',r'$U_y$']
    for i in range(4):

        bbox = axs[i][3].get_position()
        cbar_ax = fig.add_axes([0.91,bbox.y0,0.015,bbox.height])
        cb = fig.colorbar(cs[i], cax=cbar_ax)
        cb.set_label(cblabels[i],fontsize=cb_font,rotation=270,labelpad = cb_pad)
        cb.ax.tick_params(labelsize=cb_tick_font)

    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=150)

    plt.show()

def plot_predicted_U(t,pred,real,txy_grid,save,dir_save,save_name):

    """
    plot of 
    first row: true U_x, pred U_x, DIff
    second row: true U_x, pred U_x, DIff
    third row: true T, pred T, DIff
    """

    x,y = txy_grid[0,:,:,1],txy_grid[0,:,:,2]
    x, y = np.linspace(0,100,40),np.linspace(0,100,40)
    xx,yy = np.meshgrid(x,y,indexing = 'ij')

    fig, axs = plt.subplots(3,3, sharex=True, sharey=True)
    fig.set_size_inches(19.5, 15)
    
    cmaps = ['BrBG','BrBG','magma']
    cmaps_diff = ['BrBG','BrBG','RdBu']
    cblabels = [r'$U_x$',r'$U_y$',r'$T_a$']
    
    time_font = 30
    time_pad = 15
    cb_font = 30
    cb_pad = 40
    cb_tick_font = 20
    xlabel_font = 20
    x_tick_font = 20

    # find abs maxima
    abs_max = []

    for i in range(2):

        max = np.nanmax([np.nanmax(pred[i,t,:,:]),np.nanmax(real[i,t,:,:])])
        min = np.nanmin([np.nanmin(pred[i,t,:,:]),np.nanmin(real[i,t,:,:])])
        M = np.nanmax([np.abs(max),np.abs(min)])
        abs_max.append([-M,M])

    abs_max.append([0,np.nanmax([pred[2,t,:,:],real[2,t,:,:]])])
    abs_max_diff_T = max_abs(pred[2,t,:,:]- real[2,t,:,:])
    abs_max_diff_ux = max_abs(pred[0,t,:,:]- real[0,t,:,:])
    abs_max_diff_uy = max_abs(pred[1,t,:,:]- real[1,t,:,:])

    abs_max_diff = [abs_max_diff_ux,abs_max_diff_uy,abs_max_diff_T]

    cs_all = []
    for i in range(3):
        
        c1 = axs[i][0].pcolormesh(xx,yy,real[i,t,:,:],cmap=cmaps[i],vmin=abs_max[i][0],vmax=abs_max[i][1])
        c2 = axs[i][1].pcolormesh(xx,yy,pred[i,t,:,:],cmap=cmaps[i],vmin=abs_max[i][0],vmax=abs_max[i][1])
        c3 = axs[i][2].pcolormesh(xx,yy,real[i,t,:,:] - pred[i,t,:,:],cmap=cmaps_diff[i],vmin= -abs_max_diff[i],vmax=abs_max_diff[i])
        cs_all.append([c1,c2,c3])
        for j in range(3):
            axs[i][j].tick_params(axis='both',labelsize=x_tick_font)

        cb1 = fig.colorbar(c1,ax=axs[i][0])
        cb2 = fig.colorbar(c2,ax=axs[i][1])
        cb3 = fig.colorbar(c3,ax=axs[i][2])

        cb3.set_label(cblabels[i],fontsize=cb_font,rotation=270,labelpad = cb_pad)
        cb1.ax.tick_params(labelsize=cb_tick_font)
        cb2.ax.tick_params(labelsize=cb_tick_font)
        cb3.ax.tick_params(labelsize=cb_tick_font)

    titles = ['True', 'Predicted', 'Difference']

    for i in range(3):
        axs[0][i].set_title(titles[i],fontsize = time_font,pad = time_pad,fontweight='bold')

    fig.tight_layout()
    for axx in axs:
        for ax in axx:
            ax.set_xticks([])
            ax.set_yticks([])
    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=150)

    plt.show()

def plot_Ta_series(t_snaps,pred,real,txy_grid,save,dir_save,save_name):

    """
    plot of 
    rows: true T, pred T, DIff
    every t has one column
    """

    x,y = txy_grid[0,:,:,1],txy_grid[0,:,:,2]
    x, y = np.linspace(0,100,40),np.linspace(0,100,40)
    xx,yy = np.meshgrid(x,y,indexing = 'ij')
    times = txy_grid[:,0,0,0]
    n_t = len(t_snaps)

    cmaps = ['magma','magma','RdBu']
    time_font = 30
    time_pad = 15
    cb_font = 30
    cb_pad = 40
    cb_tick_font = 20
    xlabel_font = 20
    x_tick_font = 20

    fig, axs = plt.subplots(3,n_t, sharex=True, sharey=True)
    fig.set_size_inches(n_t*5.2, 15)

    M = np.nanmax([pred[2,t_snaps[-1],:,:],real[2,t_snaps[-1],:,:]])

    diffs = []
    for i,t in enumerate(t_snaps):
        diffs.append(real[2,t,:,:]-pred[2,t,:,:])  
    M_diff = max_abs(diffs)

    for i,t in enumerate(t_snaps):

        c1 = axs[0][i].pcolormesh(xx,yy,real[2,t,:,:],cmap=cmaps[0],vmin=0,vmax=M)
        c2 = axs[1][i].pcolormesh(xx,yy,pred[2,t,:,:],cmap=cmaps[1],vmin=0,vmax=M)
        c3 = axs[2][i].pcolormesh(xx,yy,real[2,t,:,:]-pred[2,t,:,:],cmap=cmaps[2],vmin=-M_diff,vmax=M_diff)
        axs[0][i].set_title('t = {}'.format(round(times[t])),fontsize = time_font,pad = time_pad,fontweight='bold')
        for j in range(3):
            axs[j][i].tick_params(axis='both',labelsize=x_tick_font)

    fig.tight_layout()
    
    fig.subplots_adjust(right=0.9)
    cs = [c1,c2,c3]
    cblabels = [r'True $T_a$',r'Predicted $T_a$','Difference']

    for i in range(3):

        bbox = axs[i][2].get_position()
        cbar_ax = fig.add_axes([0.91,bbox.y0,0.015,bbox.height])
        cb = fig.colorbar(cs[i], cax=cbar_ax)
        cb.set_label(cblabels[i],fontsize=cb_font,rotation=270,labelpad = cb_pad)
        cb.ax.tick_params(labelsize=cb_tick_font)
    
    fig.subplots_adjust(wspace=0, hspace=0)

    for axx in axs:
        for ax in axx:
            ax.set_xticks([])
            ax.set_yticks([])
    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=150)

    plt.show()

def finding_maxima_over_runs(dir,runs,t_range,star):

    """
    get the right maximum values of the U and T to plot comparisons plots over different
    runs so they all share the same colorbar values
    """

    M_Tp_l = []
    m_Tp_l = []
    M_input_ux_l = []
    M_input_uy_l = []

    for r in runs:

        pred,real,X_input_grid,U_input_grid,txy_grid = load_results(t_range,dir,r,star)
        
        M_Tp = np.nanmax(pred[2,:,:,:])
        m_Tp = np.nanmin(pred[2,:,:,:])
        if star:
            M_input_ux = max_abs(U_input_grid[:,:])
            M_input_uy = max_abs(U_input_grid[:,:])

        else:
            M_input_ux = max_abs(U_input_grid[:,:,:,0])
            M_input_uy = max_abs(U_input_grid[:,:,:,1])

        M_input_ux_l.append(M_input_ux)
        M_input_uy_l.append(M_input_uy)
        M_Tp_l.append(M_Tp)
        m_Tp_l.append(m_Tp)

    M_real_ux = max_abs(real[0,:,:,:])
    M_real_uy = max_abs(real[1,:,:,:])
    M_real_T = np.nanmax(real[2,:,:,:])
    m_real_T = np.nanmin(real[2,:,:,:])
    
    M_input_ux = max_abs(np.array(M_input_ux_l))
    M_input_uy = max_abs(np.array(M_input_uy_l))

    M_Tp = np.nanmax(np.array(M_Tp_l))
    m_Tp = np.nanmin(np.array(m_Tp_l))

    return [M_real_ux,M_real_uy,M_input_ux,M_input_uy,M_real_T,m_real_T,M_Tp,m_Tp]


def plot_defect_series(dir,run,t,t_range,defect_name,defect_l,save,dir_save,save_name):

    """
    Plot snapshot series of
    first row: true Ux, Uy, Ta
    second row: input data Ux, Uy and predicted Ta
    thrid row: similar with more noise/less resolution
    ...
    """

    # loading data
    input_ux_l = []
    input_uy_l = []
    input_X_l = []
    pred_T_l = []

    for d_i,defect in enumerate(defect_l):

        dir_d = dir + '{}_{}/'.format(defect_name,defect)
        pred,real,X_input_grid,U_input_grid,txy_grid = load_results(t_range,dir_d,run,False,dir,dir_d)

        input_ux_l.append(U_input_grid[t,:,:,0])
        input_uy_l.append(U_input_grid[t,:,:,1])
        input_X_l.append(X_input_grid)
        pred_T_l.append(pred[2,t,:,:])
        x,y = txy_grid[0,:,:,1],txy_grid[0,:,:,2]

    # finding maxima over defect runs
    M_ux = np.nanmax(np.abs(real[0,t,:,:]))
    M_uy = np.nanmax(np.abs(real[1,t,:,:]))
    M_Ta = 0.45

    # general figure
    h = 0.2
    h_r = [1,h]
    w_r = [1,1,0.3,1.,0.3]
    l_scale = 6
    for d in range(len(defect_l)):
        h_r.append(1)
    fig, axs = plt.subplots(len(defect_l)+2,5, sharex=True, sharey=True
                                ,gridspec_kw={'height_ratios': h_r, 'width_ratios': w_r})
    fig.set_size_inches(19.2, l_scale*(len(defect_l)+1+h))
    fig.subplots_adjust(wspace=0, hspace=0)

    cms = ['BrBG','magma']
    time_font = 30
    time_pad = 15
    cb_font = 25
    cb_pad = 40
    cb_tick_font = 16 
    text_font = 30
    xlabel_font = 20
    x_tick_font = 20

    shr = 1
    nbins = 5

    for i in range(5):
        axs[1, i].axis('off')
    for j in range(len(defect_l)+2):
        axs[j, 2].axis('off')
        axs[j, 4].axis('off')

    # True fields
    c1 = axs[0][0].pcolormesh(x,y,real[0,t,:,:],cmap=cms[0],vmin=-M_ux,vmax=M_ux)
    c2 = axs[0][1].pcolormesh(x,y,real[1,t,:,:],cmap=cms[0],vmin=-M_uy,vmax=M_uy)
    c3 = axs[0][3].pcolormesh(x,y,real[2,t,:,:],cmap=cms[1],vmin=0,vmax=M_Ta)

    bbox = axs[0][1].get_position()
    cbar_ax = fig.add_axes([0.5555+0.005,bbox.y0 + (bbox.height)*(1-shr)/2.,0.015,(bbox.height)*shr])
    cb = fig.colorbar(c2, cax=cbar_ax)
    cb.ax.tick_params(labelsize=cb_tick_font)
    cb.ax.yaxis.set_major_locator(MaxNLocator(nbins=nbins))

    cbar_ax = fig.add_axes([0.8333+0.007,bbox.y0 + (bbox.height)*(1-shr)/2.,0.015,(bbox.height)*shr])
    cb = fig.colorbar(c3, cax=cbar_ax)
    cb.ax.tick_params(labelsize=cb_tick_font)
    cb.ax.yaxis.set_major_locator(MaxNLocator(nbins=nbins))

    for i in range(3):
        axs[0][i].tick_params(axis='both',labelsize=x_tick_font)

    # noisy deformation data and results
    for i in range(len(defect_l)):

        M_ux = np.nanmax(np.abs(input_ux_l[i]))
        M_uy = np.nanmax(np.abs(input_uy_l[i]))
        
        x_input,y_input = input_X_l[i][0,:,:,1],input_X_l[i][0,:,:,2]
        c1 = axs[i+2][0].pcolormesh(x_input,y_input,input_ux_l[i],cmap=cms[0],vmin=-M_ux,vmax=M_ux)
        c2 = axs[i+2][1].pcolormesh(x_input,y_input,input_uy_l[i],cmap=cms[0],vmin=-M_uy,vmax=M_uy)
        c3 = axs[i+2][3].pcolormesh(x,y,pred_T_l[i],cmap=cms[1],vmin=0,vmax=M_Ta)

        bbox = axs[i+2][1].get_position()
        cbar_ax = fig.add_axes([0.5555+0.005,bbox.y0 + (bbox.height)*(1-shr)/2.,0.015,(bbox.height)*shr])
        cb = fig.colorbar(c2, cax=cbar_ax)
        cb.ax.tick_params(labelsize=cb_tick_font)
        cb.ax.yaxis.set_major_locator(MaxNLocator(nbins=nbins))

        cbar_ax = fig.add_axes([0.8333+0.007,bbox.y0 + (bbox.height)*(1-shr)/2.,0.015,(bbox.height)*shr])
        cb = fig.colorbar(c3, cax=cbar_ax)
        cb.ax.tick_params(labelsize=cb_tick_font)
        cb.ax.yaxis.set_major_locator(MaxNLocator(nbins=nbins))

        for j in range(3):
            axs[i+2][j].tick_params(axis='both',labelsize=x_tick_font)
            if defect_name == 'n_x':
                axs[i+2][1].text(0., 0.92, r'{} x {}'.format(round(40/defect_l[i]),round(40/defect_l[i])), ha='center', va='center'
                , transform=axs[i+2][1].transAxes, bbox=dict(boxstyle='round',facecolor='white', alpha=1.0),fontsize=text_font)
            else:
                axs[i+2][1].text(0., 0.92, '{}% noise'.format(round(defect_l[i]*100)), ha='center', va='center'
                , transform=axs[i+2][1].transAxes, bbox=dict(boxstyle='round',facecolor='white', alpha=1.0),fontsize=text_font)
    
    # titles
    axs[0][0].set_title(r'True $\mathbf{U_x}$',fontsize = time_font,pad = time_pad,fontweight='bold')
    axs[0][1].set_title(r'True $\mathbf{U_y}$',fontsize = time_font,pad = time_pad,fontweight='bold')
    axs[0][3].set_title(r'True $\mathbf{T_a}$',fontsize = time_font,pad = time_pad,fontweight='bold')

    axs[2][0].set_title(r'Data $\mathbf{U_x}$',fontsize = time_font,pad = time_pad,fontweight='bold')
    axs[2][1].set_title(r'Data $\mathbf{U_y}$',fontsize = time_font,pad = time_pad,fontweight='bold')
    axs[2][3].set_title(r'Predicted $\mathbf{T_a}$',fontsize = time_font,pad = time_pad,fontweight='bold')

    for axx in axs:
        for ax in axx:
            ax.set_xticks([])
            ax.set_yticks([])

    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=150, bbox_inches = 'tight')

    plt.show()


def plot_planes_series(t_snaps,pred,real,X_input,U_input,txy_grid,save,dir_save,save_name):

    """
    plot of 
    rows: 
    input U: projected planes
    pred Ta
    true Ta
    every t has a row
    """

    x,y = txy_grid[0,:,:,1],txy_grid[0,:,:,2]
    x, y = np.linspace(0,100,40),np.linspace(0,100,40)
    xx,yy = np.meshgrid(x,y,indexing = 'ij')
    times = txy_grid[:,0,0,0]
    n_t = len(t_snaps)

    x_slice,y_slice = X_input[0,:,1],X_input[0,:,2]
    U_slice = U_input

    cmaps = ['BrBG','magma','magma']
    time_font = 30
    time_pad = 15
    cb_font = 25
    cb_pad = 40
    cb_tick_font = 20
    s_scatter = 100
    xlabel_font = 20
    x_tick_font = 20

    fig, axs = plt.subplots(3,n_t, sharex=True, sharey=True)
    fig.set_size_inches(n_t*5., 15)
    fig.subplots_adjust(wspace=0, hspace=0)

    M_Ta = np.nanmax([pred[2,t_snaps[-1],:,:],real[2,t_snaps[-1],:,:]])
    M_U = np.nanmax([np.abs(U_slice[t])for t in t_snaps])

    for i,t in enumerate(t_snaps):

        c1 = axs[0][i].scatter(x_slice,y_slice,c=U_slice[t],s=s_scatter,cmap=cmaps[0],vmin=-M_U,vmax=M_U)
        c2 = axs[1][i].pcolormesh(xx,yy,pred[2,t,:,:],cmap=cmaps[1],vmin=0,vmax=M_Ta)
        c3 = axs[2][i].pcolormesh(xx,yy,real[2,t,:,:],cmap=cmaps[2],vmin=0,vmax=M_Ta)
        axs[0][i].set_title('t = {}'.format(round(times[t])),fontsize = time_font,pad = time_pad,fontweight='bold')
        for j in range(3):
            axs[j][i].tick_params(axis='both',labelsize=x_tick_font)
    
    fig.subplots_adjust(right=0.9)
    cs = [c1,c2,c3]
    cblabels = ['Projected U',r'Predicted $T_a$',r'True $T_a$']

    for i in range(3):

        bbox = axs[i][2].get_position()
        cbar_ax = fig.add_axes([0.91,bbox.y0,0.015,bbox.height])
        cb = fig.colorbar(cs[i], cax=cbar_ax)
        cb.set_label(cblabels[i],fontsize=cb_font,rotation=270,labelpad = cb_pad)
        cb.ax.tick_params(labelsize=cb_tick_font)
        
    for axx in axs:
        for ax in axx:
            ax.set_xticks([])
            ax.set_yticks([])

    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=150)

def plot_comparison_series(cases,cases_names,t_snaps,runs,alpha_eik,t_range,dir_result,save,dir_save,save_name):

    """
    comparison figure of NC-PINN results versus NC+EIK PINN results

    rows: 
    True Ta
    Reference (NC and NC+EIK)
    noise case
    projected planes
    columns:
    timestamps and LAT
    """

    # data loading
    pred_full = []

    for i,case in enumerate(cases):

        run = runs[i]

        dir_Ta = dir_result + case + '/focal_2/alpha_eik_{}/run_{}/1_Ta/'.format(alpha_eik,run)
        dir_tau = dir_result + case + '/focal_2/alpha_eik_{}/run_{}/2_tau/'.format(alpha_eik,run)
        data_dir = dir_result + case + '/focal_2/alpha_eik_{}/run_{}/1_Ta/'.format(alpha_eik,run)
        txy_dir = dir_result + case + '/focal_2/'
        if case == 'three_plane':
            star = True
        else:
            star = False
        pred_Ta,real,X_input_grid,U_input_grid,txy_grid = load_results(t_range,dir_Ta,-1,star,txy_dir,data_dir)
        pred_tau,real,X_input_grid,U_input_grid,txy_grid = load_results(t_range,dir_tau,-1,star,txy_dir,data_dir)

        pred_full.append(np.array([pred_Ta,pred_tau]))

    pred_full = np.array(pred_full) # shape (len(cases),2,4,30,40,40): 2 for NC or NC+EIK, 4 for Ux,Uy,Ta,tau

    # general
    x,y = txy_grid[0,:,:,1],txy_grid[0,:,:,2]
    x, y = np.linspace(0,100,40),np.linspace(0,100,40)
    xx,yy = np.meshgrid(x,y,indexing = 'ij')
    times = txy_grid[:,0,0,0]
    n_t = len(t_snaps)

    cmaps = ['BrBG','magma','viridis']
    time_font = 30
    time_pad = 15
    cb_font = 30
    cb_pad = 40
    cb_tick_font = 25
    nbins =5
    xlabel_font = 20
    x_tick_font = 20

    h_r = [1,0.2]
    for c in cases:
        h_r.append(1)
        h_r.append(1)
    w_r = [1]*(n_t+1)

    fig, axs = plt.subplots(len(cases)*2+2,n_t+1, sharex=True, sharey=True
    ,gridspec_kw={'height_ratios': h_r, 'width_ratios': w_r})
    fig.set_size_inches((n_t+1)*5., 5*len(cases)*2 + 5 + 1)
    fig.subplots_adjust(wspace=0, hspace=0)

    M_Ta = np.nanmax(pred_full[:,:,2,t_snaps[-1],:,:])
    M_tau = np.nanmax(pred_full[:,:,3,t_snaps[-1],:,:])
    m_tau = np.nanmin(pred_full[:,:,3,t_snaps[-1],:,:])

    # True data row
    for i,t in enumerate(t_snaps):

        cs = []

        real_Ta = real[2,t]

        c1 = axs[0][i].pcolormesh(xx,yy,real_Ta,cmap=cmaps[1],vmin = 0, vmax=M_Ta)
        cs.append(c1)

        for j in range(len(cases)):

            pred_Ta = pred_full[j,0,2,t]
            pred_tau = pred_full[j,1,2,t] 

            c1 = axs[j*2+2][i].pcolormesh(xx,yy,pred_Ta,cmap=cmaps[1],vmin = 0, vmax=M_Ta)
            c2 = axs[j*2+3][i].pcolormesh(xx,yy,pred_tau,cmap=cmaps[1],vmin = 0, vmax=M_Ta)

            axs[0][i].set_title('t = {}'.format(round(times[t])),fontsize = time_font,pad = time_pad,fontweight='bold')
            
            axs[(j)*2+2][i].tick_params(axis='both',labelsize=x_tick_font)
            axs[((j)*2)+3][i].tick_params(axis='both',labelsize=x_tick_font)
            
            cs.append(c1)
            cs.append(c2)

    real_tau = real[3,t]
    c1 = axs[0][-1].pcolormesh(xx,yy,real_tau,cmap=cmaps[2],vmin=m_tau,vmax=M_tau)

    # Results rows
    for j in range(len(cases)):

        pred_Ta = pred_full[j,0,3,t]
        pred_tau = pred_full[j,1,3,t] 

        c1 = axs[(j)*2+2][-1].pcolormesh(xx,yy,pred_Ta,cmap=cmaps[2],vmin=m_tau,vmax=M_tau)
        c2 = axs[(j)*2+3][-1].pcolormesh(xx,yy,pred_tau,cmap=cmaps[2],vmin=m_tau,vmax=M_tau)
        axs[0][-1].set_title('LAT',fontsize = time_font,pad = time_pad,fontweight='bold')
        
    # colorbar and text stuff
    axs[0][0].text(-0.5, 0.5, 'True', fontsize=40, ha="center", va="center", transform=axs[0][0].transAxes)

    for j in range(len(cases)):

        axs[(j)*2+2][0].text(0.1, 0.9, "NC", ha="left", va="top",color = 'white',
        fontsize=38, transform=axs[j*2+2][0].transAxes)
        axs[(j)*2+3][0].text(0.1, 0.9, "NC + EIK", ha="left", va="top",color = 'white',
        fontsize=38, transform=axs[j*2+3][0].transAxes)
        axs[(j)*2+2][0].text(-0.5, 0, cases_names[j], fontsize=40, ha="center", va="center", transform=axs[j*2+2][0].transAxes)

    fig.subplots_adjust(right=0.9)
    cblabels = ['Projected U',r'Predicted $T_a$',r'True $T_a$']

    bbox = axs[4][0].get_position()
    cbar_ax = fig.add_axes([0.91,bbox.y0+bbox.height*0.25,0.015,bbox.height*2.5])
    cb = fig.colorbar(cs[i], cax=cbar_ax)
    cb.set_label(r'Predicted $T_a$',fontsize=cb_font,rotation=270,labelpad = cb_pad)
    cb.ax.tick_params(labelsize=cb_tick_font)
    cb.ax.yaxis.set_major_locator(MaxNLocator(nbins=nbins))

    bbox = axs[7][2].get_position()
    cbar_ax = fig.add_axes([0.91,bbox.y0+bbox.height*0.25,0.015,bbox.height*2.5])
    cb = fig.colorbar(c1, cax=cbar_ax)
    cb.set_label(r'Predicted $\tau$',fontsize=cb_font,rotation=270,labelpad = cb_pad)
    cb.ax.tick_params(labelsize=cb_tick_font)
    cb.ax.yaxis.set_major_locator(MaxNLocator(nbins=nbins))

    for axx in axs:
        for ax in axx:
            ax.set_xticks([])
            ax.set_yticks([])

    for i in range(n_t+1):
        axs[1, i].axis('off')

    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=150)