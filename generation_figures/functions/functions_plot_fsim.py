import numpy as np
import matplotlib.pyplot as plt
import image_similarity_measures.quality_metrics as met
import os


def t_aver_errors(dir_pred,dir_real,t_range):

    """
    calculates the time averaged values of fsim or rmse

    input:
    dir: directory to the results, needs to have files like pred.npy and real.npy
    t_range: indices which to include in the calculation

    returns:
    [fsim,rmse] so two time averaged scores
    """

    pred_file = dir_pred + 'pred.npy'
    real_file = dir_real + 'real.npy'
    pred,real = np.load(pred_file), np.load(real_file)

    t_start,t_end = t_range

    pred = pred[:,t_start:t_end,:,:]
    real = real[:,t_start:t_end,:,:]

    non_nans = 0
    rmse_total = 0
    fsim_total = 0

    for t in range(len(pred[0])):

        i_real,i_pred = np.expand_dims(real[2][t],axis=-1),np.expand_dims(pred[2][t],axis=-1)

        rmse = met.rmse(i_real,i_pred)*10**(5)
        fsim = met.fsim(i_real,i_pred)

        if not np.isnan(fsim):
            non_nans += 1

            rmse_total += rmse
            fsim_total += fsim
    
    if non_nans != 0:
        rmse_total = rmse_total/non_nans
        fsim_total = fsim_total/non_nans

    return [fsim_total,rmse_total]

def t_errors(dir_pred,dir_real,t_range):

    """
    calculates the fsim or rmse values for every timestep

    input:
    dir: directory to the results, needs to have files like pred.npy and real.npy
    t_range: indices which to include in the calculation

    returns:
    [fsim,rmse] with both lists being of shape n with n len of times
    """

    pred_file = dir_pred + 'pred.npy'
    real_file = dir_real + 'real.npy'
    pred,real = np.load(pred_file), np.load(real_file)

    t_start,t_end = t_range

    pred = pred[:,t_start:t_end,:,:]
    real = real[:,t_start:t_end,:,:]

    rmse_total = []
    fsim_total = []

    for t in range(len(pred[0])):

        i_real,i_pred = np.expand_dims(real[2][t],axis=-1),np.expand_dims(pred[2][t],axis=-1)

        rmse = met.rmse(i_real,i_pred)*10**(5)
        fsim = met.fsim(i_real,i_pred)

        if not np.isnan(fsim):

            rmse_total.append(rmse)
            fsim_total.append(fsim)

        else:
            rmse_total.append(0)
            fsim_total.append(0)

    return [np.array(fsim_total),np.array(rmse_total)]

def t_aver_snr(dir_input,dir_real,t_range):

    """
    calculates the snr from the real and input data (averaged over all t's and over U_x and U_y)
    """

    input_file = dir_input + 'U_input.npy'
    real_file = dir_real + 'real.npy'
    input,real = np.load(input_file), np.load(real_file)
    input = input.reshape((30,40,40,2))
    t_start,t_end = t_range
    input = input[t_start:t_end,:,:,:]
    real = real[:,t_start:t_end,:,:]

    total_snr = 0
    total_points = 0

    for t in range(len(real[0])):

        for i in range(2):

            r = real[i,t,:,:]
            noise = r - input[t,:,:,i]

            sigma_real = np.var(r)
            sigma_noise = np.var(noise)

            snr = np.sqrt(sigma_real/sigma_noise)
            total_snr += snr
            total_points += 1

    return total_snr/total_points


def plot_fsim(dir_og,save,dir_save,save_name,x_values,x_name,pattern_names,xaxis,error_index,t_range,snr_flag,runs,mode,txt):

    """
    plot the fsim values in function of spatial resolution/noise level or architecture for both focal and spiral (lines)
    modes: 0) noise, 1) resolution, 2) NN achitecture
    """

    data_full = []
    stds_full = []
    snrs_full = []

    for pattern in pattern_names:

        data = []
        stds = []
        snr_xvalues = []

        for noise_level in x_values:

            errors = []
            snrs = []

            for run in range(runs):

                dir = dir_og + '{}/{}_{}/run_{}/'.format(pattern,xaxis,noise_level,run+1)
                dir_real = dir_og + '{}/'.format(pattern)
                error = t_aver_errors(dir,dir_real,t_range)[error_index]
                if snr_flag:
                    snr = t_aver_snr(dir,dir_real,t_range)
                    snrs.append(snr)
            
                errors.append(error)
            
            aver = np.mean(np.array(errors))
            snr = np.mean(np.array(snrs))
            std = np.std(np.array(errors))

            data.append(aver)
            snr_xvalues.append(snr)
            stds.append(std)

        data_full.append(data)
        stds_full.append(stds)
        snrs_full.append(snr_xvalues)

    data_full = np.array(data_full)
    stds_full = np.array(stds_full)
    snrs_full = np.array(snrs_full)

    fig,ax = plt.subplots()
    fig.set_size_inches(8, 5)
    s1 = 10
    s2 = 500
    sl = 18
    st = 18

    sx = 18
    sa = 13

    cs = ['sandybrown','cornflowerblue']
    if mode == 0:
        x_values = np.round(np.array(x_values)*100)
    elif mode == 1:
        x_values = (np.array(x_values)*2.5).astype(int) 
  
    for j in range(len(pattern_names)):
        if snr_flag:
            x_values = snrs_full[j]
            x_name = 'S/N ratio'
           
        data_j = data_full[j,:]
        std_j = stds_full[j,:]
        ax.plot(x_values,data_j, marker='o',c=cs[j],label='{}'.format(pattern_names[j][:-2]),alpha = 0.75,ms=s1)
        ax.fill_between(x_values,data_j-std_j,data_j+std_j,color=cs[j], alpha=0.2)
    
    ax.set_xlabel(x_name,fontsize=sx)
    ax.set_ylabel('fsim',fontsize=sx)
    ax.tick_params(axis='both', which='major', labelsize=sa)
    ax.legend(fontsize=sl,loc=4)

    if snr_flag:
       ax.invert_xaxis()

    if mode == 3:
        ax.set_title(txt,fontsize=st)
    fig.tight_layout()

    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=200)
    plt.show()


def plot_fsim_alpha_nc(dir_og,save,dir_save,save_name,alpha_nc_levels,noise_levels,error_index,t_range,runs,mode):

    """
    plot the fsim values in function of alpha_nc for different noise levels (lines) with errorbars or patterns

    mode: 0) lines are different noise levels 1) lines are different patterns
    """

    data_full = []
    std_full = []
    patterns = ['focal_2/', 'spiral_1/']

    for i,noise_per in enumerate(noise_levels):
        
        if mode == 0:
            dir_o = dir_og
        else:
            dir_o = dir_og + patterns[i]
        data = []
        std = []

        for alpha_nc in alpha_nc_levels:

            data_r = []

            for run in runs:

                dir = dir_o + 'noise_{}/alpha_nc_{}/run_{}/'.format(noise_per,alpha_nc,run)
                dir_real = dir_o
                error = t_aver_errors(dir,dir_real,t_range)[error_index]
                data_r.append(error)

            data.append(np.mean(np.array(data_r)))
            std.append(np.std(np.array(data_r)))

        data_full.append(data)
        std_full.append(std)

    data_full = np.array(data_full)
    std_full = np.array(std_full)

    fig,ax = plt.subplots()
    fig.set_size_inches(8, 5)
    s1 = 10
    s2 = 500
    sl = 18

    sx = 18
    sa = 13

    if mode == 0:
        cmap = plt.get_cmap("viridis")
        color_indices = np.linspace(0, 1, len(noise_levels))
        cs = [cmap(idx) for idx in color_indices]
    else:
        cs = ['sandybrown','cornflowerblue']
        pattern_names = ['focal','spiral']

    for j in range(len(noise_levels)):
           
        data_j = data_full[j,:]
        std_j = std_full[j,:]
        if mode == 0:
            ax.plot(alpha_nc_levels,data_j, marker='o',c=cs[j],label=r'{} $\%$'.format(round(noise_levels[j]*100)),alpha = 0.75,ms=s1)
        else:
            ax.plot(alpha_nc_levels,data_j, marker='o',c=cs[j],label='{}'.format(pattern_names[j]),alpha = 0.75,ms=s1)
        ax.fill_between(alpha_nc_levels,data_j-std_j,data_j+std_j,color=cs[j], alpha=0.2)
    
    ax.set_xscale('log')
    ax.set_xlabel(r'$\alpha_{nc}$',fontsize=sx)
    ax.set_ylabel('fsim',fontsize=sx)

    ax.tick_params(axis='both', which='major', labelsize=sa)

    if mode == 0:
        ax.legend(fontsize=sl,ncol=2,title=r'Noise level',title_fontsize = sl+2)
    else:
        ax.legend(fontsize=sl,loc=4)
    fig.tight_layout()

    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=200)
    plt.show()

def get_alpha_eik_fsim(dir_og,alpha_eik_levels,error_index,t_range):

    """
    Auxiliray function for function plot_alpha_eik_errors to get some interesting values

    0. data_full: average error values with shape (i,2)
    1. stds_full: stds of error values with shape (i,2)

    with i = len(alpha_eik_levels)
         2: methods so first NC, then EIK+NC
    """

    data_full = []
    stds_full = []

    for alpha_eik in alpha_eik_levels:

        errors_m = []
        stds_m = []

        errors_m_full = []

        for m in ['1_Ta/','2_tau/']:

            errors = []

            for run in range(5):

                dir = dir_og + 'alpha_eik_{}/run_{}/'.format(alpha_eik,run+1)
            
                dir_m = dir + m
                dir_real = dir_og
                error = t_aver_errors(dir_m,dir_real,t_range)[error_index]
                errors.append(error)
                
            aver = np.mean(np.array(errors))
            std = np.std(np.array(errors))

            errors_m.append(aver)
            stds_m.append(std)
            errors_m_full.append(errors)

        data_full.append(errors_m)
        stds_full.append(stds_m)

    data_full = np.array(data_full)
    stds_full = np.array(stds_full)

    return [data_full,stds_full]

def plot_fsim_alpha_eik(series,data_dir,save,dir_save,save_name,alpha_eik_levels,error_index,t_range,texts):

    """
    plot the fsim values in function of alpha_eik for constant alpha_tau in the EIK-PINN
    for n runs with errorbars and the constant fsim for NC-PINN
    """

    data_full = []
    stds_full = []

    for serie in series:

        dir_og = data_dir + '/{}/focal_2/'.format(serie)
        data,stds = get_alpha_eik_fsim(dir_og,alpha_eik_levels,error_index,t_range)

        data_full.append(data)
        stds_full.append(stds)

    fig,axs = plt.subplots(3,2, sharex=True, sharey=True)
    fig.set_size_inches(13, 13)

    s1 = 10
    s2 = 500
    sl = 25
    sx = 20
    sa = 16

    cs2 = ['sandybrown','olivedrab']
    labels = ['NC', 'NC + EIK']

    for i,serie in enumerate(series):

        if i >= 3:
            j = 1
            i_ax = i%3
        else:
            j = 0
            i_ax = i

        # NC
        d_j = data_full[i][:,0]
        std_j = stds_full[i][:,0] 
        axs[i_ax][j].plot(alpha_eik_levels,d_j, marker='o',c='black',label=labels[0],alpha = 0.75,ms=s1)
        axs[i_ax][j].fill_between(alpha_eik_levels,d_j-std_j,d_j+std_j,color='black', alpha=0.2)

        # Combined
        d_j = data_full[i][:,1]
        std_j = stds_full[i][:,1] 
        axs[i_ax][j].plot(alpha_eik_levels,d_j, marker='o',label=labels[1],c=cs2[0],alpha = 0.75,ms=s1)
        axs[i_ax][j].fill_between(alpha_eik_levels,d_j-std_j,d_j+std_j,color=cs2[0], alpha=0.2)
        
        
        if i_ax == 2:
            axs[i_ax][j].set_xlabel(r'$\alpha_{eik}$',fontsize=sx)
        axs[i_ax][j].set_xscale('log')
        if j == 0:
            if i_ax == 0:
                axs[i_ax][j].legend(fontsize=sl,loc=3)
            axs[i_ax][j].set_ylabel('fsim',fontsize=sx)

        axs[i_ax][j].text(0.97, 0.95,
        texts[i],
        ha = 'right', va='top',
        bbox=dict(boxstyle='round', facecolor='white', edgecolor='black'),
        fontsize=sx,
        transform=axs[i_ax][j].transAxes)
        if error_index == 0:
            axs[i_ax][j].set_ylim(0.75,1.0)
        else:
            axs[i_ax][j].set_ylim(1,3)
        axs[i_ax][j].tick_params(axis='both', which='major', labelsize=sa)
    fig.tight_layout()

    if save:
        if not os.path.exists(dir_save):
            os.makedirs(dir_save)
        plt.savefig(dir_save + save_name,dpi=200)
    plt.show()