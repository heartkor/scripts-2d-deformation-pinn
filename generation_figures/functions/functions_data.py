# functions to get data from the ithildin and fenicsx simulations and to load the saved results from PINNs

import h5py as h5
import numpy as np
import yaml
from yaml.loader import BaseLoader


def load_results(t_range,dir,run,star,txy_dir,data_dir):

    """
    load precited and true fields for U, Ta as well as the coordinates of the data points
    if star = True: load X_input and U_input as arrays (array of locations) instead of grid data
    so it can plotted it as scatter data and not pcolormesh
    """

    if run!= -1:
        pred_file = dir + 'run_{}/pred.npy'.format(run)
        real_file = txy_dir + 'real.npy'
        X_input_file = dir + 'run_{}/X_input.npy'.format(run)
        U_input_file = dir + 'run_{}/U_input.npy'.format(run)

        txy_array_file = txy_dir + 'txy_array.npy'
    else: # if the correct directory is immeditatly given
        pred_file = dir + 'pred.npy'
        real_file = txy_dir + 'real.npy'
        X_input_file = data_dir + 'X_input.npy'
        U_input_file = data_dir + 'U_input.npy'
        txy_array_file = txy_dir + 'txy_array.npy'

    t_start,t_end = t_range

    pred,real,X_input,U_input,txy_array = np.load(pred_file), np.load(real_file), np.load(X_input_file),np.load(U_input_file), np.load(txy_array_file)
    N_x = int(np.sqrt(len(txy_array)/len(pred[0])))
    txy_grid = txy_array.reshape((len(pred[0]),N_x,N_x,3))

    n_x = int(np.sqrt(len(X_input)/(len(real[0]))))
    if star:
        X_input_grid = X_input.reshape((len(real[0]),-1,3))
        U_input_grid = U_input.reshape((len(real[0]),-1))
        X_input_grid = X_input_grid[t_start:t_end,:,:]
        U_input_grid = U_input_grid[t_start:t_end,:]
    else:
        X_input_grid = X_input.reshape((len(real[0]),n_x,n_x,3))
        U_input_grid = U_input.reshape((len(real[0]),n_x,n_x,2))
        X_input_grid = X_input_grid[t_start:t_end,:,:,:]
        U_input_grid = U_input_grid[t_start:t_end,:,:,:]

    pred = pred[:,t_start:t_end,:,:]
    real = real[:,t_start:t_end,:,:]
    txy_grid = txy_grid[t_start:t_end,:,:,:]

    return [pred,real,X_input_grid,U_input_grid,txy_grid]

def load_data(pattern,pattern_n,t_range,x_range,dir_data):

    """
    Function to load all relevant arrays and data to start the PINN optimization

    Input

    pattern: string representing type, example 'focal'
    pattern_n: string respresenting the run of that pattern so the '2' in 'focal_2'
    t_range: (2,2) array which has ranges for the times inlcuded, first floats then indices
            so example [[t_fl_start,t_fl_end],[t_cut_index_start],t_cut_index_end]
    x_range: (2,) array with the spatal range, always indices (so of total 40)

    Output (with t = number of frames, N number of spatial points)

    U_grid: (t,N*N,3) ux, uy, uz for every timestep and every spatial point
    T_grid: (t,N*N), Ta for every timestep and every spatial point

    coord_grid: (N*N,2) x,y coordinates for every spatial point
    x0s: (N,N) x coordinates for every spatial point on a grid
    y0s: (N,N) y coordinates for every spatial point on a grid
    t_x_y_grid (3,t,N,N) grid of the t,x,y coordinates for all spatio_temproal points
    t_x_y_array (t*N*N,3) flattened array of the t,x,y coordinats for all spatio-temporal points
    times: (t,) all time points
    """

    t_fl_start,t_fl_end = t_range[0]
    t_index_start,t_index_end = t_range[1]
    x_index_start,x_index_end = x_range

    # loading data
    full_stem = pattern + '_' + pattern_n
    dir_deformation= dir_data + 'mechanical_data/{}_'.format(full_stem)

    # active tension
    h5_file = dir_deformation + 'active_tension_times.h5'
    T_coord_full,T_data_full = read_h5_file(h5_file,1)

    # deformation
    h5_file = dir_deformation + 'deformation_times.h5'
    u_coord_full,u_data_full = read_h5_file(h5_file,0)

    # electrical data
    directory = dir_data + 'electrical_data/'
    Sim = Sim_Ithildin(full_stem,directory)
    t_start_fl, t_stop_fl = t_fl_start,t_fl_end
    x_start_fl, x_stop_fl = 0,1
    y_start_fl, y_stop_fl = 0,1
    t_step = 2
    x_step = 5
    y_step = 5#5
    t_range = list(map(int,[Sim.t_size*t_start_fl,Sim.t_size*t_stop_fl,t_step]))
    x_range = list(map(int,[Sim.x_size*x_start_fl,Sim.x_size*x_stop_fl,x_step]))
    y_range = list(map(int,[Sim.y_size*y_start_fl,Sim.y_size*y_stop_fl,y_step]))
    electrical_subgrid = Sim.get_subgrid(t_range,x_range,y_range)
    v_grid = electrical_subgrid[0]
    times = electrical_subgrid[2]

    # getting the deformation data in the correct grid order (randomized in fenicsx output)
    u_full = u_data_full[t_index_start:t_index_end,:,:]
    T_full = T_data_full[t_index_start:t_index_end,:,:]

    N_x_full = int(np.sqrt(len(u_coord_full)))
    L = np.max(u_coord_full)
    xl = np.linspace(0,L,N_x_full)
    yl = np.linspace(0,L,N_x_full)
    x0s,y0s = np.meshgrid(xl,yl,indexing='ij')

    def get_right_order(x0s,y0s,u_coord):

        order = []
        for i in range(len(x0s.flatten())):

            x = x0s.flatten()[i]
            y = y0s.flatten()[i]

            for p,point in enumerate(u_coord):
                if abs(point[0]-x)<10**(-3) and  abs(point[1]-y)<10**(-3):
                    order.append(p)
        return order

    order = get_right_order(x0s,y0s,u_coord_full)

    u_grid = np.zeros(u_full.shape)
    T_grid = np.zeros((len(u_full),len(x0s.flatten())))
    coord_grid = np.zeros((len(x0s.flatten()),2))
    for i,index in enumerate(order):
        for t in range(len(u_full)):
            u_grid[t][i] = u_full[t][index]
            T_grid[t][i] = T_full[t][index]
        coord_grid[i] = u_coord_full[index]

    t_x_y_array = np.zeros((len(times)*len(coord_grid),3)) # [51*40*40,3]
    t_x_y_grid = [] # [3,51,40,40]

    point = 0
    for i,t in enumerate(times):
        for j,coord in enumerate(coord_grid):
            t_x_y_array[point] = np.array([t,coord[0],coord[1]])
            point += 1

    for i in range(3):
        c = t_x_y_array[:,i].reshape((len(times),len(x0s),len(y0s))) 
        t_x_y_grid.append(c)
    t_x_y_grid = np.array(t_x_y_grid)

    # now slicing in spatial domain

    # reshape all grid variabels from 1600 to 40,40
    u_grid_reshape = u_grid.reshape((len(times),N_x_full,N_x_full,3))
    T_grid_reshape = T_grid.reshape((len(times),N_x_full,N_x_full))
    v_grid_reshape = v_grid.reshape((len(times),N_x_full,N_x_full))
    coord_grid_reshape = coord_grid.reshape((N_x_full,N_x_full,2))

    # slicing according to spatial limits
    u_grid_sliced = u_grid_reshape[:,x_index_start:x_index_end,x_index_start:x_index_end,:]
    T_grid_sliced = T_grid_reshape[:,x_index_start:x_index_end,x_index_start:x_index_end]
    v_grid_sliced = v_grid_reshape[:,x_index_start:x_index_end,x_index_start:x_index_end]
    coord_grid_sliced = coord_grid_reshape[x_index_start:x_index_end,x_index_start:x_index_end,:]

    # some new spatially sliced variables
    N_x = len(coord_grid_sliced)
    L = np.max(coord_grid_sliced)
    xl = np.linspace(0,L,N_x)
    yl = np.linspace(0,L,N_x)
    x0s,y0s = np.meshgrid(xl,yl,indexing='ij')

    # reshaping back to original semi flattened array
    u_grid = u_grid_sliced.reshape((len(times),len(u_grid_sliced[0])**2,3))
    T_grid = T_grid_sliced.reshape((len(times),len(u_grid_sliced[0])**2))
    v_grid = v_grid_sliced.reshape((len(times),len(u_grid_sliced[0])**2))
    coord_grid = coord_grid_sliced.reshape((len(u_grid_sliced[0])**2,2))

    # reshaping not needed for t_x_y_grid but the first dimesnion of 3 needs to go to the last position
    t_x_y_grid = t_x_y_grid[:,:,x_index_start:x_index_end,x_index_start:x_index_end]
    t_x_y_grid_tr = np.transpose(t_x_y_grid,[1,2,3,0])
    t_x_y_array = t_x_y_grid_tr.reshape((len(times)*N_x**2,3))

    return [u_grid,T_grid,coord_grid,x0s,y0s,t_x_y_grid,t_x_y_array,times,v_grid]