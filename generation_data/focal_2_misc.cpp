std::string simseriesname = "focal";
float Pi = atan(1)*4;

float eps1 = 1.0;
float eps2 = 0.1;
float k = 8;
float a = 0.05;
float D = 1;

Model_AP3 model(a, k, eps1, eps2);
model.define_tip("u", 0.5, "v", 1.0);
//model.set_Pmat(4);

vector<int> size {200, 200, 1};
vector<float> dx {0.5, 0.5, 1};

//float kefd = 0.005;
Geometry_Iso geom = Geometry_Iso(pmpi, size, dx, &model);

float dt = 0.05;
float framedur = 1.0;
int Nframe = 100;
int Nstartrec = 0;

Sim sim = Sim(dt, framedur, Nframe, Nstartrec, pmpi, &model, &geom, simseriesname);
//sim.enable_filament_recording(20, 0.1);

geom.initialize_domain();

Source sour(&model, &geom, pmpi, &sim);

// block stimulus
float L = dx[0]*size[0];

float r = 1.0;


// focal 1

sour.schedule.emplace(0., [&]() {sour.stimulate(Stimulus{{0}, {1}, Shape::Sphere(r, {0.*L, 0.0*L})});});
sour.schedule.emplace(60., [&]() {sour.stimulate(Stimulus{{0}, {1}, Shape::Sphere(r, {0.25*L, 0.75*L})});});
sour.schedule.emplace(20, [&]() {sour.stimulate(Stimulus{{0}, {1}, Shape::Sphere(r, {0.75*L, 0.25*L})});});


//focal 2
//sour.schedule.emplace(0., [&]() {sour.stimulate(Stimulus{{0}, {1}, Shape::Sphere(r, {0.5*L, 0.5*L})});});