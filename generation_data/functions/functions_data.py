import h5py as h5
import numpy as np
import yaml
from yaml.loader import BaseLoader

class Sim_Ithildin:
    """
    Class for electrical simulation data from ithildin
    """
    def __init__(self,stem,directory):

        self.stem = stem
        self.directory = directory

        # squeezs z dim out as it is 2D data
        u = np.squeeze(np.load(self.directory + self.stem + '_u.npy'))
        v = np.squeeze(np.load(self.directory + self.stem + '_v.npy'))

        # shape(u) = (t,y,x)
        # this switches last two dim so we have (x,y)
        self.u = np.moveaxis(u, -1, -2)
        self.v = np.moveaxis(v, -1, -2)

        self.x_size = len(self.u[0,:,0])
        self.y_size = len(self.u[0,0,:])
        self.t_size = len(self.u[:,0,0])

        with open(self.directory + self.stem + '_log.yaml') as f:
            data = yaml.load(f, Loader=BaseLoader)
            
            m_p = data['Model parameters']
            self.a = float(m_p['a'])
            self.k = float(m_p['k'])
            self.eps1 = float(m_p['eps1'])
            self.eps2 = float(m_p['eps2'])

            g_p = data['Geometry parameters']
            self.dx = float(g_p['Voxel size'][0])

            s_p = data['Simulation parameters']
            self.dt = float(s_p['Frame duration'])

        self.coords = np.zeros((self.x_size,self.y_size,2))
        self.times = np.linspace(0,self.t_size-1,self.t_size)*self.dt

        for i in range(self.x_size):
            x = i*self.dx
            for j in range(self.y_size):
                y = j*self.dx
                self.coords[i][j] = np.array([x,y])

    def get_subgrid(self,t_range,x_range,y_range):

        """
        Get sparse points from the full grid

        input: ranges for t, x and y including index to start, stop and the stepsize
        output: [u,v,times,coord]
        """

        t_start,t_stop,t_step = t_range
        x_start,x_stop,x_step = x_range
        y_start,y_stop,y_step = y_range

        u = self.u[t_start:t_stop:t_step,x_start:x_stop:x_step,y_start:y_stop:y_step]
        v = self.v[t_start:t_stop:t_step,x_start:x_stop:x_step,y_start:y_stop:y_step]

        coords = self.coords[x_start:x_stop:x_step,y_start:y_stop:y_step]
        times = self.times[t_start:t_stop:t_step]

        return np.array([u,v,times,coords],dtype=object)


def read_h5_file(hf_file,n):

    if n == 0:
        name = 'Deformation'
    else:
        name = 'Active_Tension'

    hf = h5.File(hf_file, 'r')
    g= hf.get('Mesh/mesh/geometry')
    geom = g[()]

    d = hf.get('Function/{}'.format(name))
    d_time = hf.get('Function/{}/0'.format(name))

    if n ==0:
        full_data = np.zeros((len(d.keys()),len(d_time[()]),3))
    else:
        full_data = np.zeros((len(d.keys()),len(d_time[()]),1))

    for i in range(len(d.keys())):
        d_time = hf.get('Function/{}/{}'.format(name,i))
        data = d_time[()]
        full_data[i] = data

    hf.close()

    return [geom,full_data]








        


