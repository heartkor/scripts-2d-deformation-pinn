# functions to calculate the active tension from voltage (u)

import numpy as np
from scipy.integrate import odeint

"""
Gives the gradient of the active tension ode 
"""
def T_dt(T,t,u,a,kt,eps1,eps2):

    if u < a:
        eps = eps1
    else:
        eps = eps2

    return eps*(kt*u - T)

"""
Integrating the active tension ode over the t range in one spatial position
"""
def integrate_one_pos(ts,T0,u,a,k,eps1,eps2):

    Ts = np.zeros(len(ts))

    for i in range(len(ts)-1):

        t_range = [ts[i],ts[i+1]]
        T = odeint(T_dt,T0,t_range,args=(u[i],a,k,eps1,eps2))
        T0 = T[1]
        Ts[i+1] = T[1]

    return Ts

"""
Calculating the active tension on every point of the grid

input: times, u-matrix, intital T, a, kt, eps1, eps2
output: active tension in every point for every timestep (same shape as u matrix)
"""
def active_tension(ts,us,T0,a,kt,eps1,eps2):

    y_size = len(us[0,:,0])
    x_size = len(us[0,0,:])

    Ts = np.zeros((len(ts),y_size,x_size))
    iter = 0

    for y in range(y_size):

        for x in range(x_size):

            u = us[:,y,x]
            T = integrate_one_pos(ts,T0,u,a,kt,eps1,eps2)
            Ts[:,y,x] = T

            iter += 1
            if iter%100 == 0:
                print('position {}/{}'.format(iter,x_size*y_size)) 

    return Ts