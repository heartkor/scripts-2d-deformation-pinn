std::string simseriesname = "spiral";
float Pi = atan(1)*4;

float eps1 = 1.0;
float eps2 = 0.1;
float k = 8;
float a = 0.05;
float D = 1;

Model_AP3 model(a, k, eps1, eps2);
model.define_tip("u", 0.5, "v", 1.0);
//model.set_Pmat(4);

vector<int> size {200, 200, 1};
vector<float> dx {0.5, 0.5, 1};

float kefd = 0.005;
Geometry_Iso geom = Geometry_Iso(pmpi, size, dx, &model,kefd);

float dt = 0.05;
float framedur = 1.0;
int Nframe = 100;
int Nstartrec = 10;

Sim sim = Sim(dt, framedur, Nframe, Nstartrec, pmpi, &model, &geom, simseriesname);
//sim.enable_filament_recording(20, 0.1);

geom.initialize_domain();

Source sour(&model, &geom, pmpi, &sim);

// block stimulus
float L = dx[0]*size[0];


// 

sour.schedule.emplace(0.1, [&]() {sour.stimulate(Stimulus{{0}, {1}, Shape::Rect({0.5*L, 0.0*L}, {0.7*L, 0.3*L})});});
sour.schedule.emplace(0., [&]() {sour.stimulate(Stimulus{{1}, {2}, Shape::Rect({0.3*L, 0.0*L}, {0.5*L, 0.5*L})});});

