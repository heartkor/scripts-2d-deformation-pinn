# classes and functions to optimize EIK-PINN

import tensorflow as tf
import numpy as np
from pyDOE import lhs


class PINNSolver_tau():
    def __init__(self,model_tau,train_param,init_param,param_max,alphas_l,batch_size=64):

        """
        Creates an instance to solve the NC-PINN

        model_tau: Keras NN model
        train_param: list of indices for which the parameters should be optimized along the process
        init_param: initial(physics) parameters for the potantial optimization
        param_max: maximum values for the parameters
        alphas_l: list for all alpha weighting parameters
        batch_size: size of the mini-batches used in the optimization
        """

        self.model_tau = model_tau
        self.train_param = train_param
        self.batch_size = batch_size

        self.param = init_param.copy()

        self.alpha_eik = tf.constant(alphas_l[0],shape=(),dtype=tf.float32)
        self.alpha_data = tf.constant(alphas_l[1],shape=(),dtype=tf.float32)
        self.T_max = tf.constant(param_max[0],shape=(),dtype=tf.float32)
        self.tau_max = tf.constant(param_max[1],shape=(),dtype=tf.float32)

        self.model_tau(np.array([[0.,1.]])) # call it once to be able to get the model.trainable_variables in next

        self.tr_var = self.model_tau.trainable_variables
        tr_var_list = []

        for p in range(len(self.param)):
            if p in train_param:
                self.param[p] = tf.Variable(self.param[p],shape=(),trainable=True,dtype=tf.float32)
                tr_var_list.append(self.param[p])
            else:
                self.param[p] = tf.constant(self.param[p],shape=(),dtype=tf.float32)

        self.tr_var = self.tr_var + tr_var_list
        
        self.hist = [[],[],[]]
        self.iter = 0

        self.loss = 0
        self.loss_eik = 0
        self.loss_data = 0
      

    def solve_with_TFoptimizer(self, X_data, tau_data,bounds, optimizer=tf.keras.optimizers.Adam(),N=1000,seed=1234):

        X_data_tf = tf.constant(X_data,shape=X_data.shape,dtype=tf.float32)
        tau_data_tf = tf.constant(tau_data,shape=tau_data.shape,dtype=tf.float32)

        np.random.seed(seed)
        tf.random.set_seed(seed)
        tf.experimental.numpy.random.seed(seed)

        idxs_data = tf.range(tf.shape(X_data_tf)[0])

        # creating the mini-batches
        X_col_all = []
        X_data_all = []
        tau_data_all = []

        for j in range(N):
            
            # collocation points
            X_col = lhs(2,self.batch_size)

            # data points
            ridxs_data = tf.random.shuffle(idxs_data)[:self.batch_size]
            X_data_batch = tf.gather(X_data_tf, ridxs_data)
            tau_data_batch = tf.gather(tau_data_tf, ridxs_data)

            # scaling from [0,1] to physical domains
            lb_x,ub_x = bounds
            X_col = X_col*(ub_x-lb_x) + lb_x
            X_col = tf.constant(X_col,shape=X_col.shape,dtype=tf.float32)

            X_col_all.append(X_col)
            X_data_all.append(X_data_batch)
            tau_data_all.append(tau_data_batch)

        @tf.function
        def train_step(X_col,X_data,tau_data):

            loss, grad_theta, = self.get_grad(X_col,X_data, tau_data)
            optimizer.apply_gradients(zip(grad_theta, self.tr_var))
    
            return loss
        
        for i in range(N):

            X_col,X_data,tau_data = X_col_all[i],X_data_all[i],tau_data_all[i]
           
            loss = train_step(X_col,X_data,tau_data)

            self.loss = loss[0].numpy()
            self.loss_eik = loss[1].numpy()
            self.loss_data = loss[2].numpy()
            
            self.callback()

    def get_grad(self, X_col,X_data, tau_data):

        with tf.GradientTape(persistent=True) as tape:
            tape.watch(self.tr_var)
            loss = self.get_loss(X_col,X_data, tau_data)

        g = tape.gradient(loss[0], self.tr_var)
    
        del tape
        
        return [loss, g]

    def get_loss(self, X_batch, X_data, tau_data):

        # pde residual
        res_eik = self.get_r(X_batch)
        loss_eik = self.alpha_eik*tf.reduce_mean(tf.square(res_eik))
        loss = loss_eik

        # data residual
        loss_data = self.get_data_loss(X_data,tau_data)
        loss_data *= self.alpha_data
        loss += loss_data

        return [loss,loss_eik,loss_data]

    def get_data_loss(self,X_data,tau_data):

        loss_data = 0
        for i in range(len(X_data)):
            data_point = tf.expand_dims(X_data[i], 0)
            tau = tf.squeeze(self.model_tau(data_point))
            tau = tf.sigmoid(tau)*self.tau_max
            loss_data += tf.reduce_mean(tf.square(tau_data[i] - tau))

        loss_data /= len(X_data)

        return loss_data

    def get_r(self,X_batch):
        
        with tf.GradientTape(persistent=True) as tape1:

            x_col = X_batch[:,0]
            y_col = X_batch[:,1]

            tape1.watch(x_col)
            tape1.watch(y_col)

            tau = tf.squeeze(self.model_tau(tf.stack([x_col, y_col], axis=1)))
            tau = tf.sigmoid(tau)*self.tau_max

        tau_x = tape1.gradient(tau, x_col)
        tau_y = tape1.gradient(tau, y_col)

        del tape1
        
        res_eik = self.eikonal(tau_x,tau_y)

        return res_eik

    def T_Gaussian(self,t,tau):

        return self.T_max*tf.exp(-((tau-t)**2)/(2*(self.param[0]**2)))

    def eikonal(self,tau_x,tau_y):

        return tf.sqrt(tau_x**2 + tau_y **2) - (1/self.param[1])

    def get_v_from_tau(self,X_batch):

        X_batch = tf.constant(X_batch ,shape=X_batch .shape,dtype=tf.float32)

        with tf.GradientTape(persistent=True) as tape1:

            x_col = X_batch[:,0]
            y_col = X_batch[:,1]

            tape1.watch(x_col)
            tape1.watch(y_col)

            tau = tf.squeeze(self.model_tau(tf.stack([x_col, y_col], axis=1)))
            tau = tf.sigmoid(tau)*self.tau_max

        tau_x = tape1.gradient(tau, x_col)
        tau_y = tape1.gradient(tau, y_col)

        del tape1

        return 1/(tf.sqrt(tau_x**2 + tau_y**2))

    def callback(self, xr=None):

        if self.iter % 1000 == 0:
            print('It {:05d}: loss = {:10.8e}'.format(self.iter,self.loss))
        
        self.hist[0].append(self.loss)
        self.hist[1].append(self.loss_eik)
        self.hist[2].append(self.loss_data)
        
        self.iter+=1
        
