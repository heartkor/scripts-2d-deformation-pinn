# functions to get data from the ithildin and fenicsx simulations

import h5py as h5
import numpy as np
import yaml
from yaml.loader import BaseLoader

class Sim_Ithildin:

    def __init__(self,stem,directory):

        self.stem = stem
        self.directory = directory

        v = np.squeeze(np.load(self.directory + self.stem + '_u.npy')) # squeezs z dim out
        w = np.squeeze(np.load(self.directory + self.stem + '_v.npy'))

        # shape(u) = (t,y,x)
        # this switches last two dim so we have (x,y)
        self.v = np.moveaxis(v, -1, -2)
        self.w = np.moveaxis(w, -1, -2)

        self.x_size = len(self.v[0,:,0])
        self.y_size = len(self.v[0,0,:])
        self.t_size = len(self.v[:,0,0])

        with open(self.directory + self.stem + '_log.yaml') as f:
            data = yaml.load(f, Loader=BaseLoader)
            
            m_p = data['Model parameters']
            self.a = float(m_p['a'])
            self.k = float(m_p['k'])
            self.eps1 = float(m_p['eps1'])
            self.eps2 = float(m_p['eps2'])

            g_p = data['Geometry parameters']
            self.dx = float(g_p['Voxel size'][0]) # in mm

            s_p = data['Simulation parameters']
            self.dt = float(s_p['Frame duration']) # in ms

        self.coords = np.zeros((self.x_size,self.y_size,2))
        self.times = np.linspace(0,self.t_size-1,self.t_size)*self.dt

        for i in range(self.x_size):
            x = i*self.dx
            for j in range(self.y_size):
                y = j*self.dx

                self.coords[i][j] = np.array([x,y])

    def get_subgrid(self,t_range,x_range,y_range):
        """
        Get sparse points from the full grid

        input: ranges for t, x and y including index to start, stop and the stepsize
        output: [u,v,times,coord] where times is in ms and coord in mm
        """
        t_start,t_stop,t_step = t_range
        x_start,x_stop,x_step = x_range
        y_start,y_stop,y_step = y_range

        v = self.v[t_start:t_stop:t_step,x_start:x_stop:x_step,y_start:y_stop:y_step]
        w = self.w[t_start:t_stop:t_step,x_start:x_stop:x_step,y_start:y_stop:y_step]

        coords = self.coords[x_start:x_stop:x_step,y_start:y_stop:y_step]
        times = self.times[t_start:t_stop:t_step]

        return np.array([v,w,times,coords],dtype=object)


def read_h5_file(hf_file,n):

    if n == 0:
        name = 'Deformation'
    else:
        name = 'Active_Tension'

    hf = h5.File(hf_file, 'r')
    g= hf.get('Mesh/mesh/geometry')
    geom = g[()]

    d = hf.get('Function/{}'.format(name))
    d_time = hf.get('Function/{}/0'.format(name))

    if n ==0:
        full_data = np.zeros((len(d.keys()),len(d_time[()]),3))
    else:
        full_data = np.zeros((len(d.keys()),len(d_time[()]),1))

    for i in range(len(d.keys())):
        d_time = hf.get('Function/{}/{}'.format(name,i))
        data = d_time[()]
        full_data[i] = data

    hf.close()

    return [geom,full_data]

def load_data(pattern,pattern_n,t_range,x_range,dir_data):
    """
    Function to load all relevant arrays and data to start the PINN optimization

    Input

    pattern: string representing type, example 'focal'
    pattern_n: string respresenting the run of that pattern so the '2' in 'focal_2'
    t_range: (2,2) array which has ranges for the times inlcuded, first floats then indices
            so example [[t_fl_start,t_fl_end],[t_cut_index_start],t_cut_index_end]
    x_range: (2,) array with the spatal range, always indices (so of total 40)

    Output (with t = number of frames, N number of spatial points)

    U_grid: (t,N*N,3) ux, uy, uz for every timestep and every spatial point
    T_grid: (t,N*N), Ta for every timestep and every spatial point

    coord_grid: (N*N,2) x,y coordinates for every spatial point
    x0s: (N,N) x coordinates for every spatial point on a grid
    y0s: (N,N) y coordinates for every spatial point on a grid
    t_x_y_grid (3,t,N,N) grid of the t,x,y coordinates for all spatio_temproal points
    t_x_y_array (t*N*N,3) flattened array of the t,x,y coordinats for all spatio-temporal points
    times: (t,) all time points
    """
    t_fl_start,t_fl_end = t_range[0]
    t_index_start,t_index_end = t_range[1]
    x_index_start,x_index_end = x_range

    # loading data
    full_stem = pattern + '_' + pattern_n
    dir_deformation= dir_data + 'mechanical_data/{}_'.format(full_stem)

    # active tension
    h5_file = dir_deformation + 'active_tension_times.h5'
    T_coord_full,T_data_full = read_h5_file(h5_file,1)

    # deformation
    h5_file = dir_deformation + 'deformation_times.h5'
    u_coord_full,u_data_full = read_h5_file(h5_file,0)

    # electrical data
    directory = dir_data + 'electrical_data/'
    Sim = Sim_Ithildin(full_stem,directory)
    t_start_fl, t_stop_fl = t_fl_start,t_fl_end
    x_start_fl, x_stop_fl = 0,1
    y_start_fl, y_stop_fl = 0,1
    t_step = 2
    x_step = 5
    y_step = 5#5
    t_range = list(map(int,[Sim.t_size*t_start_fl,Sim.t_size*t_stop_fl,t_step]))
    x_range = list(map(int,[Sim.x_size*x_start_fl,Sim.x_size*x_stop_fl,x_step]))
    y_range = list(map(int,[Sim.y_size*y_start_fl,Sim.y_size*y_stop_fl,y_step]))
    electrical_subgrid = Sim.get_subgrid(t_range,x_range,y_range)
    v_grid = electrical_subgrid[0]
    times = electrical_subgrid[2]

    # getting the deformation data in the correct grid order (randomized in fenicsx output)
    u_full = u_data_full[t_index_start:t_index_end,:,:]
    T_full = T_data_full[t_index_start:t_index_end,:,:]

    N_x_full = int(np.sqrt(len(u_coord_full)))
    L = np.max(u_coord_full)
    xl = np.linspace(0,L,N_x_full)
    yl = np.linspace(0,L,N_x_full)
    x0s,y0s = np.meshgrid(xl,yl,indexing='ij')

    def get_right_order(x0s,y0s,u_coord):

        order = []
        for i in range(len(x0s.flatten())):

            x = x0s.flatten()[i]
            y = y0s.flatten()[i]

            for p,point in enumerate(u_coord):
                if abs(point[0]-x)<10**(-3) and  abs(point[1]-y)<10**(-3):
                    order.append(p)
        return order

    order = get_right_order(x0s,y0s,u_coord_full)

    u_grid = np.zeros(u_full.shape)
    T_grid = np.zeros((len(u_full),len(x0s.flatten())))
    coord_grid = np.zeros((len(x0s.flatten()),2))
    for i,index in enumerate(order):
        for t in range(len(u_full)):
            u_grid[t][i] = u_full[t][index]
            T_grid[t][i] = T_full[t][index]
        coord_grid[i] = u_coord_full[index]

    t_x_y_array = np.zeros((len(times)*len(coord_grid),3)) # [51*40*40,3]
    t_x_y_grid = [] # [3,51,40,40]

    point = 0
    for i,t in enumerate(times):
        for j,coord in enumerate(coord_grid):
            t_x_y_array[point] = np.array([t,coord[0],coord[1]])
            point += 1

    for i in range(3):
        c = t_x_y_array[:,i].reshape((len(times),len(x0s),len(y0s))) 
        t_x_y_grid.append(c)
    t_x_y_grid = np.array(t_x_y_grid)

    # now slicing in spatial domain

    # reshape all grid variabels from 1600 to 40,40
    u_grid_reshape = u_grid.reshape((len(times),N_x_full,N_x_full,3))
    T_grid_reshape = T_grid.reshape((len(times),N_x_full,N_x_full))
    v_grid_reshape = v_grid.reshape((len(times),N_x_full,N_x_full))
    coord_grid_reshape = coord_grid.reshape((N_x_full,N_x_full,2))

    # slicing according to spatial limits
    u_grid_sliced = u_grid_reshape[:,x_index_start:x_index_end,x_index_start:x_index_end,:]
    T_grid_sliced = T_grid_reshape[:,x_index_start:x_index_end,x_index_start:x_index_end]
    v_grid_sliced = v_grid_reshape[:,x_index_start:x_index_end,x_index_start:x_index_end]
    coord_grid_sliced = coord_grid_reshape[x_index_start:x_index_end,x_index_start:x_index_end,:]

    # some new spatially sliced variables
    N_x = len(coord_grid_sliced)
    L = np.max(coord_grid_sliced)
    xl = np.linspace(0,L,N_x)
    yl = np.linspace(0,L,N_x)
    x0s,y0s = np.meshgrid(xl,yl,indexing='ij')

    # reshaping back to original semi flattened array
    u_grid = u_grid_sliced.reshape((len(times),len(u_grid_sliced[0])**2,3))
    T_grid = T_grid_sliced.reshape((len(times),len(u_grid_sliced[0])**2))
    v_grid = v_grid_sliced.reshape((len(times),len(u_grid_sliced[0])**2))
    coord_grid = coord_grid_sliced.reshape((len(u_grid_sliced[0])**2,2))

    # reshaping not needed for t_x_y_grid but the first dimesnion of 3 needs to go to the last position
    t_x_y_grid = t_x_y_grid[:,:,x_index_start:x_index_end,x_index_start:x_index_end]
    t_x_y_grid_tr = np.transpose(t_x_y_grid,[1,2,3,0])
    t_x_y_array = t_x_y_grid_tr.reshape((len(times)*N_x**2,3))

    return [u_grid,T_grid,coord_grid,x0s,y0s,t_x_y_grid,t_x_y_array,times,v_grid]








        


