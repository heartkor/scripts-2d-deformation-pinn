# classes for the keras NN used as PINNs

import tensorflow as tf

class PINN_txy(tf.keras.Model):
    """
    Keras model for a NN with inputs (x,y,t)
    """

    def __init__(self,bounds,
            output_dim=3,
            alphaL2=10**(-8),
            num_hidden_layers=8, 
            num_neurons_per_layer=20,
            activation='tanh',
            kernel_initializer=tf.keras.initializers.GlorotNormal(seed=100),
            **kwargs):
        super().__init__(**kwargs)

        self.num_hidden_layers = num_hidden_layers
        self.num_neurons_per_layer = num_neurons_per_layer
        self.output_dim = output_dim
        self.lb_t = bounds[0]
        self.ub_t = bounds[1]
        self.lb_x = bounds[2]
        self.ub_x = bounds[3]
        
        self.scale_t = tf.keras.layers.Lambda(
            lambda x: 2.0*(x - self.lb_t)/(self.ub_t - self.lb_t) - 1.0)
        self.scale_x = tf.keras.layers.Lambda(
            lambda x: 2.0*(x - self.lb_x)/(self.ub_x - self.lb_x) - 1.0)

        self.hidden = [tf.keras.layers.Dense(
            self.num_neurons_per_layer,
                             activation=tf.keras.activations.get(activation),
                             kernel_initializer=kernel_initializer,
                             bias_regularizer=tf.keras.regularizers.L2(alphaL2),
                             kernel_regularizer=tf.keras.regularizers.L2(alphaL2))
                           for _ in range(self.num_hidden_layers)]
        self.out = tf.keras.layers.Dense(output_dim,kernel_initializer=kernel_initializer)

    def call(self, X):

        temporal_X = X[:,0:1]
        spatial_X = X[:,1:]

        temporal_Z = self.scale_t(temporal_X)
        spatial_Z = self.scale_x(spatial_X)
       
        Z = tf.concat([temporal_Z,spatial_Z], axis=1)

        for i in range(self.num_hidden_layers):
            
            Z = self.hidden[i](Z)

        return self.out(Z)


class PINN_xy(tf.keras.Model):
    """
    Keras model for a NN with inputs (x,y)
    """

    def __init__(self,bounds,
            output_dim=3,
            alphaL2=10**(-8),
            num_hidden_layers=8, 
            num_neurons_per_layer=20,
            activation='tanh',
            kernel_initializer=tf.keras.initializers.GlorotNormal(seed=100),
            **kwargs):
        super().__init__(**kwargs)

        self.num_hidden_layers = num_hidden_layers
        self.num_neurons_per_layer = num_neurons_per_layer
        self.output_dim = output_dim
        self.lb_x = bounds[0]
        self.ub_x = bounds[1]
        
        self.scale_x = tf.keras.layers.Lambda(
            lambda x: 2.0*(x - self.lb_x)/(self.ub_x - self.lb_x) - 1.0)

        self.hidden = [tf.keras.layers.Dense(self.num_neurons_per_layer,
                             activation=tf.keras.activations.get(activation),
                             kernel_initializer=kernel_initializer,
                             bias_regularizer=tf.keras.regularizers.L2(alphaL2),
                             kernel_regularizer=tf.keras.regularizers.L2(alphaL2))
                           for _ in range(self.num_hidden_layers)]
        self.out = tf.keras.layers.Dense(output_dim,kernel_initializer=kernel_initializer)

    def call(self, X):
    
        Z = self.scale_x(X)
        for i in range(self.num_hidden_layers):
            Z = self.hidden[i](Z)
        return self.out(Z)