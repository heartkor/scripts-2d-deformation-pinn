# functions to reduce the deformation data before PINN optimization

import numpy as np

def noisy_U(u_grid,noise_per,seed=1234):
    """
    Add noise to deformation components
    """
    np.random.seed(seed)
    max_Ux = np.nanmax(u_grid[:,:,0])
    max_Uy = np.nanmax(u_grid[:,:,1])
    noise_x = np.random.normal(0,max_Ux*noise_per,len(u_grid[0])*len(u_grid))
    noise_y = np.random.normal(0,max_Uy*noise_per,len(u_grid[0])*len(u_grid))
    u_grid_run = np.copy(u_grid)
    u_grid_run[:,:,0] = u_grid[:,:,0] + noise_x.reshape(u_grid[:,:,0].shape)
    u_grid_run[:,:,1] = u_grid[:,:,1] + noise_y.reshape(u_grid[:,:,1].shape)

    return u_grid_run

def make_subsample(n_x,n_t,u_grid,T_grid,coord_grid,times,N):
    """
    make a subsample of the data points

    input

    n_x: keep every n_x'th point by averaging the deformation in a nxn square
    n_t: keeping every n_t'th timepoint
    u_grid,T_grid,coord_grid,times,N: all original arrays

    output

    X_input: (N_d,2) reduced sample of coordinates
    T_input: reduced sample of active tension as input for NN
    U_input: (N_d,2) reduced sample of deformation as input for NN
    n_data_points: amount of data_points left (in space)
    t_data_points: amount of data_points left (in time)
    """
    u_coord = coord_grid.reshape((N,N,2))
    T_data = T_grid.reshape((len(times),N,N))
    u_data = u_grid.reshape((len(times),N,N,3))

    u_data = u_data.reshape((len(times),N//n_x, n_x, N//n_x,n_x, 3)).mean(axis=(2,4))
    T_data = T_data.reshape((len(times),N//n_x, n_x, N//n_x,n_x)).mean(axis=(2,4))
    u_coord = u_coord[::n_x,::n_x,:]
    u_coord = u_coord[:,:] + (0.5*(n_x-1)*2.5)

    n_data_points = len(u_coord)
    # going back to semi flattened array
    u_coord = u_coord.reshape((len(u_coord))**2,2)
    T_data = T_data.reshape((len(times),int(n_data_points**2)))
    u_data = u_data.reshape((len(times),int(n_data_points**2),3))

    u_data = u_data[::n_t,:,0:2]
    T_data = T_data[::n_t,:]
    times_data = times[::n_t]

    n_time_points = len(u_data)

    X_input = np.zeros((int(len(u_data)*len(u_coord)),3))
    U_input = np.zeros((int(len(u_data)*len(u_coord)),2))
    T_input = np.zeros((int(len(u_data)*len(u_coord)),1))

    point = 0
    for i,t in enumerate(times_data):
        for j,c in enumerate(u_coord):

            X_input[point][:] = np.array([t,c[0],c[1]])
            U_input[point][:] = np.array([u for u in u_data[i][j]])
            T_input[point][:] = np.array([T_data[i][j]])
            point += 1

    return [X_input,T_input,U_input,n_data_points,n_time_points]

def filter_radial_lines(n_grid, theta_start, n_lines, n_points):
    """
    Only retain data that lies on lines through the middle.

    input

    n_grid: size of grid
    theta_start: angle where the first line should be with respect to the positive y axis
    n_lines: number of lines
    n_points: number of points per line

    output
    
    row, cols: indices for the rows and cols to slice any n x n grid
    e_ls: direction for the lines
    """
    center = n_grid / 2
    rows = []
    cols = []
    e_ls = []
    
    for i in range(n_lines):
        angle = (theta_start + (360 / n_lines) * i) % 360
        radians = np.deg2rad(angle)
        e_l = [np.sin(radians),np.cos(radians)]
        
        for j in range(n_points):
            radius = (j * (n_grid-1)) / (n_points - 1)
            x = center + radius * np.sin(radians)
            y = center + radius * np.cos(radians)
            
            x_rounded = round(x)
            y_rounded = round(y)
            
            if 0 <= x_rounded < n_grid and 0 <= y_rounded < n_grid:
                rows.append(x_rounded)
                cols.append(y_rounded)
                e_ls.append(e_l)
    
    return np.array(rows), np.array(cols), np.array(e_ls)

def line_variables(row,cols,X_input,T_input,U_input,times,N):
    """
    Use the rows and cols from filter_radial_lines to slice the original arrays
    """
    u_coord = X_input.reshape((len(times),N,N,3))
    T_data = T_input.reshape((len(times),N,N))
    u_data = U_input.reshape((len(times),N,N,2))

    u_coord = u_coord[:,row,cols,:]
    T_data = T_data[:,row,cols]
    u_data = u_data[:,row,cols,:]

    u_coord = u_coord.reshape(len(times)*len(row),3)
    T_data= T_data.reshape(len(times)*len(row))
    u_data = u_data.reshape(len(times)*len(row),2)

    return [u_coord,T_data,u_data]

def get_tau_from_T(T,th,times,N,tile):

    tau = np.zeros((N,N))
    tau.fill(np.nan)
    for j in range(N):
        for k in range(N):
           
            series = T[:,j,k]
            index = np.argmax(series)
            max_Ta = series[index]
            t = times[index]
            if max_Ta >= th:
                tau[j,k] = t
        
    if tile:
        return np.tile(tau,(len(times),1,1))
    else:
        return tau