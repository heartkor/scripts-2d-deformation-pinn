# functions to save results

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from functions.functions_reduction_data import get_tau_from_T

def plot_losses(hist,loss_names,name,save):

    for i in range(len(hist)):
        plt.plot(hist[i],label=loss_names[i])

    plt.yscale('log')
    plt.xlabel('Iteration')
    plt.ylabel('Global losses')
    plt.legend()
    if save:
        plt.savefig(name,dpi=200)
    plt.close()
    plt.show()

def prep_for_saving_Ta(output,times,x0s,y0s,u_grid,T_grid,solver,th):
    """
    Transform the raw outputs of the trained models towards something that can be saved

    Outputs real and predicted fields for U_x, U_y, Ta and tau
    So pred for example has size [4,t,n,n] with t the number of time points and n the number of spatial points
    """
    from itertools import repeat

    # predicting variables
    ux,uy,Ta = np.array(output[:,0]),np.array(output[:,1]),np.array(output[:,2])  
    T = tf.sigmoid(Ta)*solver.T_max
    T = T.numpy()
    
    ux = ux.reshape(len(times),len(x0s),len(y0s))
    uy = uy.reshape(len(times),len(x0s),len(y0s))
    T = T.reshape(len(times),len(x0s),len(y0s))

    tau_pred = get_tau_from_T(T,th,times,len(x0s),tile=True)

    pred = np.array([ux,uy,T,tau_pred])

    # the truth value variables
    ux_real = u_grid[:,:,0].reshape(len(times),len(x0s),len(y0s))
    uy_real = u_grid[:,:,1].reshape(len(times),len(x0s),len(y0s))
    T_real = T_grid.reshape(len(times),len(x0s),len(y0s))
    tau_real = get_tau_from_T(T_real,th,times,len(x0s),tile=True)
    real = np.array([ux_real,uy_real,T_real,tau_real])

    return [pred,real]

def prep_for_saving_tau(output_Ta,output_tau,times,x0s,y0s,u_grid,T_grid,solver_tau,th):
    """
    Transform the raw outputs of the trained models towards something that can be saved

    Outputs real and predicted fields for U_x, U_y, Ta and tau
    So pred for example has size [4,t,n,n] with t the number of time points and n the number of spatial points
    """
    from itertools import repeat

    ux,uy,Ta = np.array(output_Ta[:,0]),np.array(output_Ta[:,1]),np.array(output_Ta[:,2])  
    ux = ux.reshape(len(times),len(x0s),len(y0s))
    uy = uy.reshape(len(times),len(x0s),len(y0s))

    tau = output_tau
    tau = np.array(tf.squeeze(tau))
    tau = tau.reshape((len(x0s),len(x0s)))
    tau = np.tile(tau,(len(times),1,1))
    tau = tau.flatten()
    
    repeated_times = np.array([elem for elem in times for _ in repeat(None, int(len(tau)/len(times)))])
    tau = tf.constant(tau,shape=tau.shape,dtype=tf.float32)
    repeated_times = tf.constant(repeated_times,shape=repeated_times.shape,dtype=tf.float32)
    tau = tf.sigmoid(tau)*solver_tau.tau_max
    T = solver_tau.T_Gaussian(tau,repeated_times)
    T = T.numpy()
    tau = tau.numpy()

    T = T.reshape(len(times),len(x0s),len(y0s))
    tau = tau.reshape(len(times),len(x0s),len(y0s))

    pred = np.array([ux,uy,T,tau])

    # the truth value variables
    ux_real = u_grid[:,:,0].reshape(len(times),len(x0s),len(y0s))
    uy_real = u_grid[:,:,1].reshape(len(times),len(x0s),len(y0s))
    T_real = T_grid.reshape(len(times),len(x0s),len(y0s))
    tau_real = get_tau_from_T(T_real,th,times,len(x0s),tile=True)
    real = np.array([ux_real,uy_real,T_real,tau_real])

    return [pred,real]

def save_log(dir,noise_per,n_x,n_t,n_layers_u,n_neurons_u,batch_size,N,alphas,alphas_name,seed):
    """
    Saving some general information
    """
    with open(dir, 'w') as f:
            f.write('Std of noise for deformation input: {}\n'.format(noise_per))
            f.write('Every {} data points in the x and y direction and every {} time points\n'.format(n_x,n_t))
            f.write('Neural network: layers {}, nodes {}\n'.format(n_layers_u,n_neurons_u))
            f.write('Optimization: batchsize {}, iterations {}\n'.format(batch_size,N))
            f.write('Numpy and tensorflow seed {}\n'.format(seed))
            for q,alpha in enumerate(alphas):
                f.write('{} {}\n'.format(alphas_name[q],alpha))