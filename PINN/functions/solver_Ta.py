# classes and functions to optimize NC-PINN

import tensorflow as tf
import math
import numpy as np
from pyDOE import lhs

class PINNSolver_Ta():
    def __init__(self, model,train_param,init_param,param_max,alphas_l,projected,batch_size=64):

        """
        Creates an instance to solve the NC-PINN

        model: Keras NN model
        train_param: list of indices for which the parameters should be optimized along the process
        init_param: initial(physics) parameters for the potantial optimization
        param_max: maximum values for the parameters
        alphas_l: list for all alpha weighting parameters
        projected: boolean indicating if the data is projected (scalar deformation data)
        batch_size: size of the mini-batches used in the optimization
        """

        self.model = model
        self.train_param = train_param
        self.batch_size = batch_size
        self.projected = projected

        self.param = init_param.copy()

        self.alpha_nc = tf.constant(alphas_l[0],shape=(),dtype=tf.float32)
        self.alpha_data = tf.constant(alphas_l[1],shape=(),dtype=tf.float32)
        self.alpha_bound_nc = tf.constant(alphas_l[2],shape=(),dtype=tf.float32)
        self.alpha_Ta = tf.constant(alphas_l[3],shape=(),dtype=tf.float32)

        self.T_max = tf.constant(param_max[0],shape=(),dtype=tf.float32)
        self.l_max = tf.constant(param_max[1],shape=(),dtype=tf.float32)
        self.mu_max = tf.constant(param_max[2],shape=(),dtype=tf.float32)

        self.model(np.array([[0.,1.,2.]])) # call it once to be able to get the model.trainable_variables in next

        self.tr_var = self.model.trainable_variables
        tr_var_list = []

        logit = lambda x,max: -math.log(max/x-1)
        self.param[0] = logit(self.param[0],self.l_max.numpy())
        self.param[1] = logit(self.param[1],self.mu_max.numpy())

        for p in range(len(self.param)):
            if p in train_param:
                self.param[p] = tf.Variable(self.param[p],shape=(),trainable=True,dtype=tf.float32)
                tr_var_list.append(self.param[p])
            else:
                self.param[p] = tf.constant(self.param[p],shape=(),dtype=tf.float32)

        self.tr_var = self.tr_var + tr_var_list
    
        self.hist = [[],[],[],[],[]]
        self.iter = 0

        self.loss = 0
        self.loss_nc = 0
        self.loss_data = 0
        self.loss_bound_nc = 0
        self.loss_reg = 0


    def solve_with_TFoptimizer(self, X_data, U_data,bounds, e_ps = [], optimizer=tf.keras.optimizers.Adam(),N=1000,seed=1234):
    
        X_data_tf = tf.constant(X_data,shape=X_data.shape,dtype=tf.float32)
        U_data_tf = tf.constant(U_data,shape=U_data.shape,dtype=tf.float32)
        e_ps_data_tf = tf.constant(e_ps,shape=e_ps.shape,dtype=tf.float32)

        np.random.seed(seed)
        tf.random.set_seed(seed)
        tf.experimental.numpy.random.seed(seed)

        idxs_data = tf.range(tf.shape(X_data_tf)[0])

        # creating the mini-batches
        X_col_all = []
        X_bound_all = []
        X_data_all = []
        U_data_all = []
        e_ps_all = []

        for j in range(N):

            # collocation points
            X_col = lhs(3,self.batch_size)

            # boundary points
            s = int(self.batch_size/4)
            X_bound = lhs(3, self.batch_size)
            X_bound[0:s,1] = 0.
            X_bound[s:s*2,2] = 0.
            X_bound[s*2:s*3,1] = 1.
            X_bound[s*3:s*4,2] = 1.

            # data points
            ridxs_data = tf.random.shuffle(idxs_data)[:self.batch_size]
            X_data_batch = tf.gather(X_data_tf, ridxs_data)
            U_data_batch = tf.gather(U_data_tf, ridxs_data)
            e_ps_data_batch = tf.gather(e_ps_data_tf,ridxs_data)

            # scaling from [0,1] to physical domains
            lb_t,ub_t,lb_x,ub_x = bounds
            
            X_col[:,1:] = X_col[:,1:]*(ub_x-lb_x) + lb_x
            X_col[:,0:1] = X_col[:,0:1]*(ub_t-lb_t) + lb_t
            X_bound[:,1:] = X_bound[:,1:]*(ub_x-lb_x) + lb_x
            X_bound[:,0:1] = X_bound[:,0:1]*(ub_t-lb_t) + lb_t

            X_col = tf.constant(X_col,shape=X_col.shape,dtype=tf.float32)
            X_bound = tf.constant(X_bound,shape=X_bound.shape,dtype=tf.float32)

            X_col_all.append(X_col)
            X_bound_all.append(X_bound)
            X_data_all.append(X_data_batch)
            U_data_all.append(U_data_batch)
            e_ps_all.append(e_ps_data_batch)
      
        @tf.function
        def train_step(X_col,X_bound,X_data,U_data, e_ps):

            loss, grad_theta, = self.get_grad(X_col,X_bound,X_data, U_data, e_ps)
            optimizer.apply_gradients(zip(grad_theta, self.tr_var))
    
            return loss
        
        for i in range(N):

            X_col,X_bound,X_data,U_data, e_ps = X_col_all[i],X_bound_all[i],X_data_all[i],U_data_all[i], e_ps_all[i]
           
            loss = train_step(X_col,X_bound,X_data,U_data, e_ps)

            self.loss = loss[0].numpy()
            self.loss_nc = loss[1].numpy()
            self.loss_data = loss[2].numpy()
            self.loss_bound_nc = loss[3].numpy()
            self.loss_reg = loss[4].numpy()
            self.callback()

    def get_grad(self, X_col,X_bound,X_data, U_data, e_ps):

        with tf.GradientTape(persistent=True) as tape:
            tape.watch(self.tr_var)
            loss = self.get_loss(X_col,X_bound,X_data, U_data, e_ps)
        
        g = tape.gradient(loss[0], self.tr_var)
        del tape
        
        return [loss, g]

    def get_loss(self, X_batch,X_bound, X_data, U_data, e_ps):

        # pde residual
        res_ncx,res_ncy,reg_Ta = self.get_r(X_batch)
        loss_nc = self.alpha_nc*(tf.reduce_mean(tf.square(res_ncx)) + tf.reduce_mean(tf.square(res_ncy)))
        loss = loss_nc + reg_Ta

        # data residual
        if not self.projected:
            loss_data = self.get_data_loss(X_data,U_data)
        else:
            loss_data = self.get_projected_data_loss(X_data,U_data,e_ps)

        loss_data *= self.alpha_data
        loss += loss_data

        # bounds for displacement
        loss_bound_nc = 0
        for i in range(len(X_bound)):

            data_point = tf.expand_dims(X_bound[i], 0)
            output = self.model(data_point)
            ux_pred,uy_pred,T = output[:,0],output[:,1],output[:,2]

            loss_bound_nc += tf.reduce_mean(tf.square(0 - ux_pred))
            loss_bound_nc += tf.reduce_mean(tf.square(0 - uy_pred))

        loss_bound_nc /= len(X_bound)
        loss_bound_nc *= self.alpha_bound_nc
        loss += loss_bound_nc
       
        return [loss,loss_nc,loss_data,loss_bound_nc,reg_Ta]

    def get_data_loss(self,X_data,U_data):

        loss_data = 0
        for i in range(len(X_data)):
            data_point = tf.expand_dims(X_data[i], 0)
            output = self.model(data_point)
            ux_pred,uy_pred,T = output[:,0],output[:,1],output[:,2]
            loss_data += tf.reduce_mean(tf.square(U_data[i][0] - ux_pred))
            loss_data += tf.reduce_mean(tf.square(U_data[i][1] - uy_pred))

        loss_data /= len(X_data)

        return loss_data

    def get_projected_data_loss(self,X_data,U_data,e_ps):

        loss_data = 0
        for i in range(len(X_data)):
            data_point = tf.expand_dims(X_data[i], 0)
            output = self.model(data_point)
            ux_pred,uy_pred,T = output[:,0],output[:,1],output[:,2]
            up_pred = ux_pred*e_ps[i][0] + uy_pred*e_ps[i][1]
            loss_data += tf.reduce_mean(tf.square(U_data[i] - up_pred))

        loss_data /= len(X_data)

        return loss_data


    def get_r(self,X_batch):
        
        with tf.GradientTape(persistent=True) as tape1:
    
            t_col = X_batch[:,0]
            x_col = X_batch[:,1]
            y_col = X_batch[:,2]
            tape1.watch(x_col)
            tape1.watch(y_col)
            tape1.watch(t_col)

            with tf.GradientTape(persistent=True) as tape2:

                tape2.watch(x_col)
                tape2.watch(y_col)
                tape2.watch(t_col)

                output = self.model(tf.stack([t_col,x_col, y_col], axis=1))
                ux,uy,T= output[:,0],output[:,1],output[:,2]

                T = tf.sigmoid(T)*self.T_max
                
            # first derivatives
            ux_x = tape2.gradient(ux, x_col)
            uy_y = tape2.gradient(uy, y_col)
            ux_y = tape2.gradient(ux, y_col)
            uy_x = tape2.gradient(uy, x_col)
            div = ux_x + uy_y

            T_x = tape2.gradient(T, x_col)
            T_y = tape2.gradient(T, y_col)

        # second derivatives
        ux_xx = tape1.gradient(ux_x,x_col)
        ux_yy = tape1.gradient(ux_y,y_col)
        uy_xx = tape1.gradient(uy_x,x_col)
        uy_yy = tape1.gradient(uy_y,y_col)

        div_x = tape1.gradient(div,x_col)
        div_y = tape1.gradient(div,y_col)

        del tape1
        del tape2

        # Navier cauchy residual
        res_ncx = self.nc_equation(div_x,ux_xx + ux_yy,T_x)
        res_ncy = self.nc_equation(div_y,uy_yy + uy_xx,T_y)

        # regularization of T_a
        reg_Ta = self.alpha_Ta*tf.reduce_mean(tf.math.abs(T_x) + tf.math.abs(T_y))

        return [res_ncx,res_ncy,reg_Ta]

    def nc_equation(self,t1,t2,t3):
        """Residual of the NC PDE"""

        l = tf.sigmoid(self.param[0])*self.l_max
        mu = tf.sigmoid(self.param[1])*self.mu_max
    
        return (l + mu)*t1 + (mu*t2) + t3

    def callback(self, xr=None):

        if self.iter % 1000 == 0:
            print('It {:05d}: loss = {:10.8e}'.format(self.iter,self.loss))
        
        self.hist[0].append(self.loss)
        self.hist[1].append(self.loss_nc)
        self.hist[2].append(self.loss_data)
        self.hist[3].append(self.loss_bound_nc)
        self.hist[4].append(self.loss_reg)
    
        self.iter+=1
        
