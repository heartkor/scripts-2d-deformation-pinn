#  script to create and optimize NC-PINN
import numpy as np
import tensorflow as tf

import sys
import os

current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

from functions.functions_sim_data import load_data
from functions.functions_reduction_data import noisy_U,make_subsample,filter_radial_lines,line_variables
from functions.functions_save import plot_losses,prep_for_saving_Ta,save_log
from functions.models_general import PINN_txy
from functions.solver_Ta import PINNSolver_Ta

# input parameters
seed = 1234
n_runs = 10
seed_l = [seed+s for s in range(n_runs)]

stem = 'focal'
pattern_n = '2'

# data ranges
t_cut_fl_1 = 0
t_cut_fl_2 = 0.6
t_cut_index_1 = 0
t_cut_index_2 = 30

x_cut_1 = 0
x_cut_2 = 41

# nc
l = 1.25
mu = 1
l_max,mu_max = 5,3

# eikonal
sigma = 2.5
v = 1.5

# other
T_max = 0.42 
train_param = []
th = T_max/3.

# NN params
eps = 10**(-7)
lr = 10**(-3)

batch_size = 64
N = 2000
n_layers = 15
n_neurons = 35

# data
dir_data = '../../data/'
noise_per = 0.
n_x = 1
n_t = 1

# alphas
alpha_nc = 10**(2)
alpha_data = 1
alpha_bound_nc = 1
alpha_L2 = 0
alpha_Ta = 0

# tri-planar projections
lines = False
n_points = 40
n_lines = 6
theta_start = 0
projected = False

# saving settings
save = True
save_dir_og = 'results/test/' + stem + '_' + pattern_n + '/'

######################################################################
###################### MAIN LOOP #####################################
######################################################################

for run,seed in enumerate(seed_l):

    save_dir = save_dir_og + 'run_{}/'.format(run+1)

    # loading data
    t_range = [[t_cut_fl_1,t_cut_fl_2],[t_cut_index_1,t_cut_index_2]]
    x_range = [x_cut_1,x_cut_2]

    u_grid,T_grid,coord_grid,x0s,y0s,t_x_y_grid,t_x_y_array,times,v_grid = load_data(stem,pattern_n,t_range,x_range,dir_data)
    N_x = len(x0s)

    # bounds
    lb_x = np.min(coord_grid)
    ub_x = np.max(coord_grid)
    lb_t = np.min(times)
    ub_t = np.max(times)
    bounds_u = [lb_t,ub_t,lb_x,ub_x]
    bounds_tau = [lb_x,ub_x]
    tau_max = ub_t

    # gathering lists
    alphas_names = ['alpha_nc','alpha_data','alpha_bound_nc','alpha_Ta']
    alphas = [alpha_nc,alpha_data,alpha_bound_nc,alpha_Ta]
    param = [l,mu,v]
    param_max = [T_max,l_max,mu_max]
    loss_names = ['Total','nc','data','bound_nc','regularisation']

    # saving directory
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)

    if save:
        data_file = save_dir_og + 'txy_array.npy'
        np.save(data_file,t_x_y_array)
        
    # noise added
    u_grid_run = noisy_U(u_grid,noise_per,seed=seed)

    # Reduce the resolution
    X_input_run,T_input_run,U_input_run,n_data_points,n_time_points = make_subsample(n_x,n_t,u_grid_run,T_grid,coord_grid,times,N_x)
    
    # or take projected tri-planar data 
    e_ps = np.zeros((N_x*N_x*len(times),2))
    if lines:
        row,cols,e_ps = filter_radial_lines(N_x,theta_start,n_lines,n_points)
        e_ps = np.tile(e_ps,(len(times),1))
        X_input_run,T_input_run,U_input_run = line_variables(row,cols,X_input_run,T_input_run,U_input_run,times,N_x)
    
    if projected:
        U_input_run_new = np.zeros(len(U_input_run))
        for k,U_data_point in enumerate(U_input_run):
            U_input_run_new[k] = np.dot(U_data_point,e_ps[k])
        U_input_run = U_input_run_new

    # saving log file
    log_file = save_dir + '/logfile.txt'
    if save:
        save_log(log_file,noise_per,n_x,n_t,n_layers,n_neurons,batch_size,N,alphas,alphas_names,seed)

    # saving the used sata points
    if save:
        data_file = save_dir + '/X_input.npy'
        np.save(data_file,X_input_run)
        data_file = save_dir + '/U_input.npy'
        np.save(data_file,U_input_run)

    # optimization
    model = PINN_txy(bounds_u,3,alpha_L2,n_layers,n_neurons,kernel_initializer=tf.keras.initializers.GlorotNormal(seed=seed))
    solver = PINNSolver_Ta(model,train_param,param,param_max,alphas,projected,batch_size)
    optimizer=tf.keras.optimizers.Adam(learning_rate=lr,epsilon=eps)
    solver.solve_with_TFoptimizer(X_input_run,U_input_run,bounds_u,e_ps,optimizer=optimizer,N=N,seed=seed)

    # plotting loss
    loss_plot_name = save_dir + '/loss.png'
    plot_losses(solver.hist,loss_names,loss_plot_name,save)

    # saving results    
    if save:
        output = solver.model(t_x_y_array)
        pred,real = prep_for_saving_Ta(output,times,x0s,y0s,u_grid,T_grid,solver,th)
        data_file = save_dir + '/pred.npy'
        np.save(data_file,pred)
        data_file = save_dir_og + '/real.npy'
        np.save(data_file,real)

    print('Finished run {}'.format(run))