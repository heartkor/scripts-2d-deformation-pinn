Python scripts used in the work "Reconstruction of Excitation Waves from Mechanical Deformation using Physics-Informed Neural Networks" (https://doi.org/10.48550/arXiv.2401.03948). It consists of the scripts to generate the synthetic datasets with ithildin, scipy and fenicsx, code to create and optimize the NC-PINN or EIK-PINN and notebooks to produce the figures from the paper. All necessary data and PINN results can also be found in the zenodo archive 10.5281/zenodo.10557024, which should be saved in the parent directory of this repo in order to run the PINN optimization or figure creation shown in the paper.

generation_data

- misc files to initialize the ithildin simulation (https://gitlab.com/heartkor/ithildin hash id 122fe90ccbb362d2da1452386f0248374f05e3c9)
- notebook to produce active tension from the voltage with scipy package and deformation with fenicsx. Assumes you have the necessary data in the parent directory of this repo.
- auxiliary functions
- important package versions are: dolfinx 0.4.1, ffcx 0.4.2, basix 0.4.2, ufl 2019.1.0, scipy 1.3.3, h5py 3.8.0, yaml 5.3.1, Petsc4py 3.12.0

PINN

- main python scripts to create and optimize the NC-PINN or the NC+EIK PINNs in the combined approach. Assumes you have the necessary data in the parent directory of this repo.
- auxiliary functions including reading in data and saving results and classes for the NN tensorflow keras models and custom optimizers
- requirements.txt contains the necessary python packages (notably Tensforflow 2.10.0, pyDOE 0.3.8 and image_similarity_measures 0.3.5)

generation figures

- notebooks to generate all the figures from the paper. Assumes you have the necessary results in the parent directory of this repo.
- auxiliary functions
- same requirements.txt file as for the PINNs
